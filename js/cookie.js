"use strict";

function createCookie(name, value, days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    var expires = "; expires=" + date.toGMTString();
  } else {
    var expires = "";
  }

  document.cookie = name + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];

    while (c.charAt(0) == ' ') {
      c = c.substring(1, c.length);
    }

    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }

  return null;
}

function eraseCookie(name) {
  createCookie(name, "", -1);
}

var desktopCookie = readCookie('desktop');

if (desktopCookie) {
  var viewport = document.getElementById('metaviewport'),
      wrapper = document.querySelector('html');
  viewport.setAttribute('content', 'width=1025'); //viewport.setAttribute('minimum-scale', '1.1');

  document.addEventListener('DOMContentLoaded', function (event) {
    var e = document.querySelector(".app-footer__full-version > a"),
        wr = document.querySelector(".app-footer__full-version");

    if (e !== null) {
      wr.classList.add('mobile');
      e.classList.add('mobile');
      e.innerHTML = "Мобильная версия сайта";
    }
  });
}