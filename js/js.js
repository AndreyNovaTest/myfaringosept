function initAptekaSlider(){
	$('.s-apteka-slider').each(function() {
		var el = $(this); //общий контейнер, в котором лежат и слайдер, и стрелки
		var slider = el.find('.s-apteka-slider');
		var nextArrow = el.find('.slider__owl-next');
		var prevArrow = el.find('.slider__owl-prev');
		
		slider.owlCarousel({
			// параметы
			loop:false,
		    margin:3,
		    dots: false,
		    nav:false,
		    smartSpeed:700,
			autoWidth:true,
		    responsive:{
		        0:{
		            items:1,
		            margin:15,
		        },
		        320:{
		            items:1,
		            margin:15,
		        },
		        350:{
		            items:3,
		            margin:15,
		        },
		        769:{
		            items:4,
		            margin:12,
		        },
		        1000:{
		            items:7,
		            margin:15,
		        }
		    }
		});
		
		nextArrow.click(function(){
			slider.trigger('next.owl.carousel');
		});
		
		
		prevArrow.click(function(){
			slider.trigger('prev.owl.carousel');
		});

		/**Arrow**/
		var item = $('.owl-item');

		if(item.length > 7){
			$(".slider__boxPrev").addClass('slider__boxPrev-tablet-hidden');
			$(".slider__boxNext").addClass('slider__boxPrev-tablet-hidden');

		}
		else{
			$(".slider__boxPrev").addClass('slider__boxPrev-hidden');
			$(".slider__boxNext").addClass('slider__boxPrev-hidden');
			console.log(item.length);
		}
		/**Arrow***/
	});	
}




$(document).ready(function(){
	initAptekaSlider();
});




		

	







