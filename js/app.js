"use strict";

/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */
// getCookie(), setCookie(), deleteCookie()


// возвращает cookie если есть или undefined
function getCookie(name) {

    var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

// уcтанавливает cookie
function setCookie(name, value, props) {

    props = props || {};

    var exp = props.expires;

    if (typeof exp == "number" && exp) {

        var d = new Date();

        d.setTime(d.getTime() + exp * 1000);

        exp = props.expires = d;
    }

    if (exp && exp.toUTCString) {
        props.expires = exp.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in props) {

        updatedCookie += "; " + propName;

        var propValue = props[propName];

        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

// удаляет cookie
function deleteCookie(name) {

    setCookie(name, null, { expires: -1 });
}
/*
Аргументы:

name
название cookie
value
значение cookie (строка)
props
Объект с дополнительными свойствами для установки cookie:
expires
Время истечения cookie. Интерпретируется по-разному, в зависимости от типа:
Если число - количество секунд до истечения.
Если объект типа Date - точная дата истечения.
Если expires в прошлом, то cookie будет удалено.
Если expires отсутствует или равно 0, то cookie будет установлено как сессионное и исчезнет при закрытии браузера.
path
Путь для cookie.
domain
Домен для cookie.
secure
Пересылать cookie только по защищенному соединению.
*/

/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */

/**
 * Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
 * Она сработает только один раз через N миллисекунд после последнего вызова.
 * Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
 * первого запуска.
 */
function debounce(func, wait, immediate) {
  var timeout = null,
      context = null,
      args = null,
      later = null,
      callNow = null;
  return function () {
    context = this;
    args = arguments;

    later = function later() {
      timeout = null;

      if (!immediate) {
        func.apply(context, args);
      }
    };

    callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  };
} // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license


;

(function () {
  var lastTime = 0,
      vendors = ['ms', 'moz', 'webkit', 'o'],
      x,
      currTime,
      timeToCall,
      id;

  for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback) {
      currTime = new Date().getTime();
      timeToCall = Math.max(0, 16 - (currTime - lastTime));
      id = window.setTimeout(function () {
        callback(currTime + timeToCall);
      }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };
  }
})();

;

(function () {
  // Test via a getter in the options object to see if the passive property is accessed
  var supportsPassiveOpts = null;

  try {
    supportsPassiveOpts = Object.defineProperty({}, 'passive', {
      get: function get() {
        window.supportsPassive = true;
      }
    });
    window.addEventListener('est', null, supportsPassiveOpts);
  } catch (e) {} // Use our detect's results. passive applied if supported, capture will be false either way.
  //elem.addEventListener('touchstart', fn, supportsPassive ? { passive: true } : false);

})();

function getSVGIconHTML(name, tag, attrs) {
  if (typeof name === 'undefined') {
    console.error('name is required');
    return false;
  }

  if (typeof tag === 'undefined') {
    tag = 'div';
  }

  var classes = 'svg-icon svg-icon--<%= name %>';
  var iconHTML = ['<<%= tag %> <%= classes %>>', '<svg class="svg-icon__link">', '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<%= name %>"></use>', '</svg>', '</<%= tag %>>'].join('').replace(/<%= classes %>/g, 'class="' + classes + '"').replace(/<%= tag %>/g, tag).replace(/<%= name %>/g, name);
  return iconHTML;
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };
  /**
   * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
   */

  var $remodalFrom = $('.remodal form');
  var $remodalSuccess = $('<div class="mf-ok-text">Спасибо, ваше сообщение принято.</div>');

  $remodalFrom.on('submit', function(){
      BX.addCustomEvent('onAjaxSuccess', function(){
        $remodalFrom.find('input[type="text"]').val('');
        $remodalFrom.find('textarea').val('');
        $remodalSuccess.prependTo($remodalFrom);  
      }); 
  });

  $('._js-articles-menu-title').on('click', function (event) {
    event.preventDefault();
    $(this).closest('.b-articles__menu').toggleClass('is-opened').find('>ul').stop().slideToggle();
  });
  $('._js-faq-accordion-header').on('click', function (event) {
    event.preventDefault();
    $(this).closest('.b-faq__item').toggleClass('is-opened').find('.b-faq__body').stop().slideToggle();
  });
  $('._js-form-control').on('change', function (event) {
    if ($(this).val() == "") {
      $(this).closest('.b-form__item').removeClass('is-active');
    } else {
      $(this).closest('.b-form__item').addClass('is-active');
    }
  });

  if ($('#webgl').length && $('#preloader').length && $('#preloader_icon').length) {
    var loadingPageFunction = function loadingPageFunction() {
      $('html.is-loading').removeClass('is-loading');
      $('.b-preloader').remove();

      if ($('body').hasClass('app--main')) {
        var fullpageHeight = function fullpageHeight(elem) {
          var heightWindow = $(window).outerHeight();
          elem.css('height', heightWindow);
        };

        var resizeFunction = function resizeFunction() {
          fullpageHeight($('.block-wrapper'));
        };

        var $fullpageMain = $('._js-fullpage'),
            $draggedIndex = 0,
            draggedFlag = false;
        $fullpageMain.owlCarousel({
          items: 1,
          loop: false,
          nav: false,
          dots: false,
          autoHeight: false,
          mouseDrag: false,
          animateOut: 'fadeOut',
          onTranslate: function onTranslate() {
            cssAnimation(true);
            setTimeout(function () {
              $('.animator-hide').removeClass('animator-hide');
            }, 1000);
          },
          onDragged : function() {
              if ($fullpageMain.find('> .owl-stage-outer > .owl-stage > .owl-item').length == $draggedIndex) {
                $draggedIndex = $draggedIndex - 1;
              $('.b-callback-main').removeClass('footerView');
              $('#points_content').addClass('pass').removeClass('footerpass');
              setTimeout(function(){
                $fullpageMain.trigger('to.owl.carousel', [$draggedIndex, 300]);
              }, 50);
              setTimeout(function(){
                CLOUD.pageAnimation($draggedIndex,function (){NAVI.animatePlane($draggedIndex); cssAnimation();});
              }, 200);
              } else {
              $draggedIndex = $fullpageMain.find('> .owl-stage-outer > .owl-stage > .owl-item.active').index();

              if (draggedFlag == true && ($fullpageMain.find('> .owl-stage-outer > .owl-stage > .owl-item').length - $draggedIndex) == 1) {
                $draggedIndex = $draggedIndex+1;
                $('.b-callback-main').addClass('footerView');
                $('#points_content').removeClass('pass').addClass('footerpass');
                setTimeout(function(){
                  CLOUD.pageAnimation($draggedIndex,function (){NAVI.animatePlane($draggedIndex); cssAnimation();});
                }, 200);
              } else {
                $('.b-callback-main').removeClass('footerView');
                $('#points_content').addClass('pass').removeClass('footerpass');
                setTimeout(function(){
                  CLOUD.pageAnimation($draggedIndex,function (){NAVI.animatePlane($draggedIndex); cssAnimation();});
                }, 200);
              }
            }

            if ($fullpageMain.find('> .owl-stage-outer > .owl-stage > .owl-item').length - $draggedIndex == 1) {
              draggedFlag = true;
            } else {
              draggedFlag = false;
            }
            fullpageHeight($('.block-wrapper'));
          }
        });
        var scrollReturnSlide = 0;
        $fullpageMain.on('wheel', '.owl-stage-outer', function (e) {
          // if (!$(e.target).closest('.b-article-main__item-in').length) {
          if (scrollReturnSlide == 0) {
            clearTimeout($.data(this, 'timer'));
            $.data(this, 'timer', setTimeout(function () {
              var itemLength = $fullpageMain.find('> .owl-stage-outer > .owl-stage > .owl-item').length;
              scrollReturnSlide = 1;

              if (e.originalEvent.deltaY > 0 && $draggedIndex < itemLength) {
                $fullpageMain.trigger('next.owl');
              } else if (e.originalEvent.deltaY < 0 && $draggedIndex != 0 && $draggedIndex < itemLength) {
                $fullpageMain.trigger('prev.owl');
              } else if (e.originalEvent.deltaY < 0 && $draggedIndex >= itemLength) {
                $draggedIndex -= 1;
                cssAnimation(true);
                $('.b-callback-main').removeClass('footerView');
                $('.b-callback-main__form-box').css('height', '');
                $('.b-main-footer__in').removeClass('flexNone');
                $('#points_content').removeClass('footerpass');

                if (!readCookie('contraindicationsHide')) {
                  $('.b-contraindications').removeClass('hiden');
                } else {
                  $('#points_content').addClass('pass');
                }

                fullpageHeight($('.block-wrapper'));
                CLOUD.pageAnimation($draggedIndex, function () {
                  NAVI.animatePlane($draggedIndex);
                  cssAnimation();
                });
                cssAnimation();
                $draggedIndex = $fullpageMain.find('> .owl-stage-outer > .owl-stage > .owl-item.active').index();
                return false;
              } else {
                e.preventDefault();
                return false;
              }

              cssAnimation(true);
              setTimeout(function () {
                var $thisIndex = $fullpageMain.find('> .owl-stage-outer > .owl-stage > .owl-item.active').index();

                if (itemLength - $draggedIndex == 1 && itemLength - $thisIndex == 1) {
                  $('.b-callback-main').addClass('footerView');
                  $('#points_content').addClass('footerpass');
                  $('#points_content').removeClass('pass').addClass('footerpass');
                  $('.b-contraindications').addClass('hiden');
                  var footerHeight = $('.b-callback-main .app-footer').height(),
                      winHeight = $(window).height(),
                      contentForm = $('.form-callback').height() + $('.b-callback-main__form-info').height(),
                      contentHeight = winHeight - footerHeight;
                  $('.b-callback-main__form-box').css('height', contentHeight);

                  if (contentForm > contentHeight) {
                    $('.b-main-footer__in').addClass('flexNone');
                  } else {
                    $('.b-main-footer__in').removeClass('flexNone');
                  }

                  $thisIndex += 1;
                } 

                CLOUD.pageAnimation($thisIndex, function () {
                  NAVI.animatePlane($thisIndex);
                  cssAnimation();
                }); 

                cssAnimation();
                $draggedIndex = $thisIndex;
              }, 100);
              setTimeout(function () {
                scrollReturnSlide = 0;
              }, 1500);
            }, 250));
            e.preventDefault();
          } 

        });
        $('.next-step, .b-first-symptoms__next-step').on('click', function (e) {
          e.preventDefault();
          var $this = $(this),
              $thisIndex = $this.closest('.owl-item').index() + 1;
          $draggedIndex = $thisIndex;
          $fullpageMain.trigger('to.owl.carousel', [$thisIndex, 300]); 

          CLOUD.pageAnimation($thisIndex, function () {
            NAVI.animatePlane($thisIndex);
            cssAnimation();
          }); 
        });
        $('#points_content .point').on('click', function (e) {
          e.preventDefault();
          var $this = $(this),
              $thisIndex = $this.index() - 1,
              itemLength = $fullpageMain.find('> .owl-stage-outer > .owl-stage > .owl-item').length;

          if ($thisIndex >= itemLength) {
            $('.b-callback-main').addClass('footerView');
            var footerHeight = $('.b-callback-main .app-footer').height(),
                winHeight = $(window).height(),
                contentForm = $('.form-callback').height() + $('.b-callback-main__form-info').height(),
                contentHeight = winHeight - footerHeight;
            $('.b-callback-main__form-box').css('height', contentHeight);

            if (contentForm > contentHeight) {
              $('.b-main-footer__in').addClass('flexNone');
            } else {
              $('.b-main-footer__in').removeClass('flexNone');
            }
            $draggedIndex = $thisIndex;
          } else {
            $('.b-callback-main').removeClass('footerView');
            $('.b-callback-main__form-box').css('height', '');
            $('.b-main-footer__in').removeClass('flexNone');
            $draggedIndex = $thisIndex;
            fullpageHeight($('.block-wrapper'));
          }

          $fullpageMain.trigger('to.owl.carousel', [$thisIndex, 300]); // if ($thisIndex == 1) {
          // 	CLOUD.pageAnimation($thisIndex,function (){NAVI.animatePlane($thisIndex); setTimeout( function(){cssAnimation()}, 1000);});
          // } else {

          CLOUD.pageAnimation($thisIndex, function () {
            NAVI.animatePlane($thisIndex);
            cssAnimation();
          }); // }

          if ($thisIndex >= itemLength) {
            $('#points_content').removeClass('pass').addClass('footerpass');
            $('.b-contraindications').addClass('hiden');
          } else {
            $('#points_content').removeClass('footerpass');

            if (!readCookie('contraindicationsHide')) {
              $('.b-contraindications').removeClass('hiden');
            } else {
              $('#points_content').addClass('pass');
            }
          }
        });
        $(window).on('resize', resizeFunction).trigger('resize');
        $('.b-contraindications__eyes').on('click', function (e) {
          e.preventDefault();
          $('#points_content').addClass('pass');
          $('.b-contraindications').addClass('hiden');
          createCookie('contraindicationsHide', 1, 1);
          return false;
        });

        if (readCookie('contraindicationsHide')) {
          $('#points_content').addClass('pass');
          $('.b-contraindications').addClass('hiden');
        }
      }
    };

    CLOUD.INIT //Анимация заднего фона
    ({
      host: document.getElementById("webgl"),
      //Хост для сцены
      preloader: document.getElementById("preloader"),
      //блок прелоудера
      preloaderIcon: document.getElementById("preloader_icon"),
      //Блок, где меняются проценты
      content: content,
      //массив с адресами к ресурсам (Оставить как есть, все в файле content.js)
      urls: {
        models: "models/",
        textures: "/bitrix/templates/faringosept/webgl/textures/",
        images: "/bitrix/templates/faringosept/webgl/textures/"
      },
      //Пути к папкам с ресурсами
      preloadingEndCb: function preloadingEndCb() {
        loadingPageFunction();
      } //Коллбэк после полной загрузки

    });
    NAVI.INIT({
      //Анимация навигации
      host: document.getElementById("points_content"),
      //Хост для навигации
      planeScr: "/bitrix/templates/faringosept/webgl/images/plane_icon.png",
      //картинка самолета
      btns: document.getElementsByClassName("point") // Массив с html кнопками, которые расположены поверх навигации, на них пользователь кликает и наводит мышь 

    });
  } else {
    $('html.is-loading').removeClass('is-loading');
    $('.b-preloader').remove();
    cssAnimation(true);
    cssAnimation();
  }

  $('.b-kinds-kids__tabs-title > ul li').on('click', function (e) {
    e.preventDefault();
    var $this = $(this),
        $slideIndex = $this.index();
    $('.b-kinds-kids__tabs-title > ul li').not($this).removeClass('active');
    $('.b-kinds-kids__tabs-body').removeClass('active');
    $('.b-kinds-kids__tabs-body:eq(' + $slideIndex + ')').addClass('active');
    $this.addClass('active');
    cssAnimation();
  });
  $('.b-kinds__tabs-title > ul li').on('click', function (e) {
    e.preventDefault();
    var $this = $(this),
        $slideIndex = $this.index();
    $('.b-kinds__tabs-title > ul li').not($this).removeClass('active');
    $('.b-kinds__tabs-body').removeClass('active');
    $('.b-kinds__tabs-body:eq(' + $slideIndex + ')').addClass('active');
    $this.addClass('active');
    cssAnimation();
  });

  function mainFooter() {
    var footerHeight = $('.b-main-footer .app-footer').height(),
        winHeight = $(window).height(),
        contentForm = $('.b-main-footer__form-content').height(),
        contentHeight = winHeight - footerHeight;
    $('.b-main-footer__in').css('height', contentHeight);

    if (contentForm > contentHeight) {
      $('.b-main-footer__in').addClass('flexNone');
    } else {
      $('.b-main-footer__in').removeClass('flexNone');
    }
  }

  $('._js_toggle-mobile-nav').on('click', function () {
    $('.b-mobile-nav').toggleClass('is-opened');
  });
  $(window).on('resize', function () {
    if (window.matchMedia('(min-width: 731px').matches) {
      if ($('.b-mobile-nav.is-opened').length) {
        $('.b-mobile-nav').toggleClass('is-opened');
      }
    }
  });
  $('._js-tabs-title').on('click', function (event) {
    event.preventDefault();

    if ($(this).hasClass('is-active')) {
      return;
    }

    var indx = $(this).data('tabs-index');
    $('._js-tabs-title.is-active').removeClass('is-active');
    $(this).addClass('is-active');
    $('._js-tabs-body').removeClass('is-active').hide();
    $('._js-tabs-body[data-tabs-index="' + indx + '"]').fadeIn();
  });
  ;

  $('video').mediaelementplayer({
      success: function success(player, node) {

          // Optional
          $(player).closest('.mejs__container').attr('lang', mejs.i18n.language());

          $('html').attr('lang', mejs.i18n.language());

          // More code
      }
  });
  $('.cookie__btn').on('click', function(e) {
    e.preventDefault();

    $('.cookie__open').removeClass('show');
    createCookie('cookieClose', 1, 1);
    return false;
  });
  if (!readCookie('cookieClose')) {
    $('.cookie__open').addClass('show');
  }
  $('.anchor-link').on('click', function(e){
    e.preventDefault();
      if ($('html, body').is(':animated')) {
          return false;
      }
      var hrefBlock = $(this).data('anchor'),
          elemBlock = $('.'+hrefBlock);

      if ($(elemBlock).length) {
          var indent = 20;
          var k = false;

          $('html, body').animate({
              scrollTop: $(elemBlock).offset().top - indent
          }, 600, function () {
              setTimeout(function () {
                  k = true;
              }, 100);
          });
      }
      return false;
  });
  $('._js-show-products').on('click', function (event) {
    event.preventDefault();
    $(this).slideUp();
    $('.b-section--products-hide-mobile').slideDown();
  });
  $('._js-iamspecialist').on('click', function (event) {
    event.preventDefault();
    setCookie('iamspecialist', '1', {
      expires: 30
    });
    window.location.reload();
  });
  $('._js-data-pic').each(function () {
    var $this = $(this),
        pic = $this.data('pic');
    $this.css('background-image', 'url(' + pic + ')');
  });
  var testimonyAdaptationForkidsFlag = 1;

  function testimonyAdaptationForkids() {
    if (window.matchMedia('(max-width:767px)').matches && testimonyAdaptationForkidsFlag == 1) {
      $('.b-section--testimony-forkids').after($('._js-testimony-text'));
      testimonyAdaptationForkidsFlag = 2;
    } else if (window.matchMedia('(min-width:768px)').matches && testimonyAdaptationForkidsFlag == 2) {
      $('.b-section--testimony-forkids .b-section__description').after($('._js-testimony-text'));
      testimonyAdaptationForkidsFlag = 1;
    }
  }

  $(window).on('resize', testimonyAdaptationForkids).trigger('resize');
  var testimonyAdaptationFlag = 1;

  function testimonyAdaptation() {
    if (window.matchMedia('(max-width:767px)').matches && testimonyAdaptationFlag == 1) {
      $('.b-section--testimony').after($('._js-testimony-items'));
      testimonyAdaptationFlag = 2;
    } else if (window.matchMedia('(min-width:768px)').matches && testimonyAdaptationFlag == 2) {
      $('.b-section--testimony .b-section__header').after($('._js-testimony-items'));
      testimonyAdaptationFlag = 1;
    }
  }

  $(window).on('resize', testimonyAdaptation).trigger('resize');

  $('._js-source-header').on('click', function () {
    $(this).closest('.b-source').toggleClass('is-opened').find('.b-source__body').stop().slideToggle();
    var bodyMaxHeight = $('.app-footer').height() - $(this).outerHeight();
    $(this).closest('.b-source').find('.b-source__body').css('maxHeight', bodyMaxHeight);
    $(this).closest('.b-source').find('.b-source__body').css('minHeight', bodyMaxHeight);
  });
  var $tasteSlider = $('._js-taste-slider');

  $tasteSlider.owlCarousel({
      items: 1,
      margin: 40,
      center: false,
      loop: false,
      nav: false,
      dots: true,
      startPosition: 0,
      autoHeight: false,
      mouseDrag: true,
      touchDrag: false,
      onChanged : function() {
      $tasteSlider.find('.owl-item .b-tastes__slider-item-name').css('opacity', 0);
      },
      onInitialized : function() {
        if ($tasteSlider.find('.owl-item.center').length) {
          var activeSlide = $tasteSlider.find('.owl-item.center').index();
        $tasteSlider.find('.owl-item.center .b-tastes__slider-item-name').css('opacity', 1);
        } else {
          var activeSlide = $tasteSlider.find('.owl-item.active').index();
        $tasteSlider.find('.owl-item.active .b-tastes__slider-item-name').css('opacity', 1);
        }
      $('.b-tastes__tabs-box > ul li:eq('+activeSlide+')').addClass('active');
      },
      onTranslated : function() {
        if ($tasteSlider.find('.owl-item.center').length) {
          var activeSlide = $tasteSlider.find('.owl-item.center').index();
        $tasteSlider.find('.owl-item.center .b-tastes__slider-item-name').css('opacity', 1);
              $('.owl-item .b-tastes__slider-item-name').removeClass('animator animator-s2 animator-slideInDown');
        } else {
          var activeSlide = $tasteSlider.find('.owl-item.active').index();
        $tasteSlider.find('.owl-item.active .b-tastes__slider-item-name').css('opacity', 1);
        }
      $('.b-tastes__tabs-box > ul li').removeClass('active');
      $('.b-tastes__tabs-box > ul li:eq('+activeSlide+')').addClass('active');
      },
      responsive : {
        760: {
          items: 2,
          startPosition: 2,
          mouseDrag: true,
          center: true
        }
      }
  });

  function tasteSliderResize() {
      $tasteSlider.owlCarousel('refresh');
      if ($tasteSlider.find('.owl-item.center').length) {
          var activeSlide = $tasteSlider.find('.owl-item.center').index();
          $tasteSlider.find('.owl-item.center .b-tastes__slider-item-name').css('opacity', 1);
          $('.owl-item .b-tastes__slider-item-name').removeClass('animator animator-s2 animator-slideInDown');
      } else {
          var activeSlide = $tasteSlider.find('.owl-item.active').index();
          $tasteSlider.find('.owl-item.active .b-tastes__slider-item-name').css('opacity', 1);
      }
  }
  $(window).on('resize', tasteSliderResize).trigger('resize');

  $('.b-tastes__tabs-box > ul li').on('click', function(e){
    e.preventDefault();

    var $this = $(this),
      $slideIndex = $this.index();
    $('.b-tastes__tabs-box > ul li').not($this).removeClass('active');
    $('._js-taste-slider .owl-dots > div:eq('+$slideIndex+')').trigger('click');
    $this.addClass('active');
  });
  $('._js-usefularticles-menu-title').on('click', function (event) {
    event.preventDefault();
    $(this).closest('.b-useful-articles__menu').toggleClass('is-opened').find('>ul').stop().slideToggle();
  });

/* если находимся на внутренних страницах */
  if ($('#webgl2').length) {
    CLOUD.SIMPLEINIT//Анимация заднего фона
      ({
        host: document.getElementById("webgl2"),
        preloadingEndCb: function (){
          //$('#webgl2').addClass('ready');
        },

        urls:{models:"models/",textures:"/bitrix/templates/faringosept/webgl/textures/",images:"/bitrix/templates/faringosept/webgl/textures/"},//Пути к папкам с ресурсами
      });      
  }
  /* */
});