var LOADER = {
	counter:0,
	max :0,
	result:{models:{},textures:{},images:{}},
	callbackLoadingEnd : function (r){},
	callbackLoadingProgress : function (p){},
	INIT : function (content,urls,cbEnd,cbProcess)
	{
	
		if(cbEnd)LOADER.callbackLoadingEnd = cbEnd;
		if(cbProcess)LOADER.callbackLoadingProgress = cbProcess;
		LOADER.result.models = {};
		if(content.models)LOADER.max += content.models.length;
		if(content.textures)LOADER.max += content.textures.length;
		if(content.images)LOADER.max += content.images.length;
		var modelCounter = 0;
		var textureCounter = 0;
		var imagesCounter = 0;

		loadOne(content.models[modelCounter],"model");
		loadOne(content.textures[textureCounter],"texture");
		loadOne(content.images[imagesCounter],"image");
		function loadOne (data,type)
		{
			if(!data)return;
			if(type === "model")
			{
				
				new THREE.JSONLoader().load(urls.models + data.file,function(geometry, materials){
					modelCounter++;
					loadOne(content.models[modelCounter],"model");
					LOADER.result.models[data.name] = geometry;
					LOADER.checkLoadingProgress();
				});
			}
			else if(type === "texture")
			{
				
				var l = new THREE.TextureLoader();
				l.load(urls.textures + data.file,function(texture){
					
					textureCounter++;
					loadOne(content.textures[textureCounter],"texture");
					LOADER.result.textures[data.name] = texture;
					checkLoadingProgress();
				});
			}
			else if(type === "image")
			{
				
				var img =  new Image();
				img.src = urls.images + data.file;
				img.onload = function (){
					imagesCounter++;
					loadOne(content.images[imagesCounter],"image");
					LOADER.result.images[data.name] = img;
					checkLoadingProgress();
				}

			}
		}
		function checkLoadingProgress ()
		{
			
			LOADER.counter++;
			LOADER.callbackLoadingProgress(LOADER.counter/LOADER.max);
			if(LOADER.counter >= LOADER.max)
			{
				LOADER.callbackLoadingEnd(LOADER.result);
			}
		}
	}
}