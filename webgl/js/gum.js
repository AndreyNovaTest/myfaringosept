var GUM = {
	deltaCount : function (count,min,max)
	{
		return (count - min)/(max-min);
	},
	lerp : function (p,start,end)
	{
		return start + p*(end-start);
	},
	cos : function (count,type)
	{
		if(type === undefined)
		{
			return ((Math.cos(Math.PI - count*Math.PI))/2)+.5;
		}
		else if(type === 0)
		{
			return (((Math.cos(Math.PI - (count/2*Math.PI)))/2)+.5)*2;
		}
		else if(type === 1)
		{
			return (((-1 + Math.cos(Math.PI - (.5 + count/2)*Math.PI))/2)+.5)*2;
		}
	
		
	},

	atan : function (count)
	{
		return (0.7919956 + Math.atan(-1 + count*2))*Math.PI/5;
		
	},
	
	cCurv : function (count)
	{
		
		return GUM.arr[Math.round(count*100)];
	},
	arr : [0,0.00007594875187199888, 0.0003069761432499852, 0.0006996338892499584, 0.0012592305719059185, 0.0019928479999998653, 0.002905092952749809, 0.004005694000000141, 0.005301904880750585, 0.006800920192001147, 0.008509865218751828, 0.010442575754706638, 0.012604201506002598, 0.015006593750001851, 0.01766278083122693, 0.020582287860221826, 0.023779519863460538, 0.02726470970270305, 0.03105925593524134, 0.035169033628969434, 0.0396236983217093, 0.044436692017894984, 0.049621205572713836, 0.055204806448001346, 0.06120975556800755, 0.06766730775924049, 0.07460265356602218, 0.08204185165761468, 0.09002079668903802, 0.09856806741854628, 0.10774204249885548, 0.11756536579576964, 0.1281013033398369, 0.13938502135868325, 0.15148487601766875, 0.16444993986603754, 0.17836519701488757, 0.19328328545028303, 0.20926871398849792, 0.22642437814489225, 0.2448036331522381, 0.26446988049212944, 0.28552421379113846, 0.30796824163473485, 0.33184933045897264, 0.35714835594179356, 0.38378812510587346, 0.4116297391335618, 0.4404857097968282, 0.4700459520484921, 0.49997000000055486, 0.5299091197006521, 0.5594695745436319, 0.5883258911695411, 0.6161679726837819, 0.6428083173118854, 0.6681080114895228, 0.6919898475810711, 0.7144483874094784, 0.7354898875618406, 0.7551832592464536, 0.7735628287438391, 0.7907063467385326, 0.8067045752834799, 0.8216229995183021, 0.8355271326059934, 0.8485040027178626, 0.860604201845086, 0.8718882653978967, 0.8824245493449266, 0.8922482189011812, 0.9014225402802086, 0.90997920331003, 0.9179494459294838, 0.9253889875908167, 0.9323246758532513, 0.9387825690430172, 0.9447878574222324, 0.9503787944263703, 0.955563307981205, 0.9603699741154685, 0.9648249717931161, 0.9689407440639408, 0.9727299559283052, 0.9762204801357881, 0.9794177121390629, 0.9823372191680951, 0.9849893713678618, 0.9873957984934002, 0.9895574242447385, 0.9914870560970382, 0.9931990798075314, 0.994698095118828, 0.9959943059996261, 0.9970949070469263, 0.9980071519997264, 0.9987407694278728, 0.9993003661105826, 0.9996930238566372, 0.999924051248071,1],
	animationPool:{},
	addAnimation : function (name,f,max)
	{
		GUM.animationPool[name] = {
			func:f,
			count:0,
			max:max
		}
	},
	loopPool : {},
	addLoop : function (name,f,max)
	{
		f();
		GUM.loopPool[name] = {
			func:f,
			count:0,
			max:max
		}
	},
	removeLoop : function (name)
	{
		
		delete GUM.loopPool[name];
	},
	dom:{},
	addDom : function(atribute,name)
	{
		var elem = document.getElementById(atribute);
		if(elem)
		{
			GUM.dom[name] = elem;
			return;
		}
		var elems = document.getElementsByClassName(atribute);
		if (elems.length > 0)
		{
			GUM.dom[name] = elems;
			return;
		}
	},
	removeAnimation : function (name,toEnd)
	{
		if(!GUM.animationPool[name])return;
		var tempPool = GUM.animationPool[name]
		delete GUM.animationPool[name];
		if(toEnd)tempPool.func(1,tempPool.max);
		;
	},
	update : function ()
	{
		for(var value in GUM.animationPool)
		{
			if(GUM.animationPool[value].count < GUM.animationPool[value].max)
			{
				GUM.animationPool[value].count++;
				GUM.animationPool[value].func(GUM.animationPool[value].count/GUM.animationPool[value].max,GUM.animationPool[value].count);
				
			}
			else
			{
				GUM.removeAnimation(value);
			}
			
		}
	},
	loopUpdate : function ()
	{
		for(var value in GUM.loopPool)
		{
			if(GUM.loopPool[value].count < GUM.loopPool[value].max)
			{
				GUM.loopPool[value].count++;
				
				
			}
			else
			{
				GUM.loopPool[value].count = 0;
				GUM.loopPool[value].func(1);
			}
			
		}
	},
	customUpdate : function ()
	{
		GUM.loopUpdate();
		GUM.update();
	},
	setDefUpdate : function (num)
	{
		
		
		GUM.defNum += num;
		GUM.definitiveUpdate();
	},
	defNum:0,
	definitiveUpdate : function ()
	{
		if(GUM.defNum > 0)
		{
			GUM.defNum--;
			GUM.loopUpdate();
			GUM.update();
			requestAnimationFrame(GUM.definitiveUpdate);
			console.log(GUM.defNum)
		}
		
	},
	autoUpdate : function ()
	{
		GUM.loopUpdate();
		GUM.update();
		//setTimeout(function (){requestAnimationFrame(GUM.autoUpdate);},50);
		requestAnimationFrame(GUM.autoUpdate);
	}
}