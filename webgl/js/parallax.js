var PRLX = {
	scene : null,
	camera : null,
	light : null,
	planes : [],
	renderer : null,
	cameraOrigin : 200,
	cameraDistance : 260,
	textures : [],
	displacement : [],
	gap : 0,
	host:null,
	alphaT: .9,
	mDepth: 50,
	sunAngle : 3.14,
	sunHeight:35,
	sunIntens : .5,
	hemyIntens : .5,
	subdiv:128,
	depthData :[],
	defineData : {},
	background : 0x000000,
	backgroundAlpha : 0,

	depth : 37,
	create : function(data)
	{
		//Сцена
		PRLX.scene = new THREE.Scene();
		//Рендер
		PRLX.renderer = new THREE.WebGLRenderer({antialias:true,alpha: true});
		PRLX.host = document.getElementById(data.host);
		PRLX.host.appendChild(PRLX.renderer.domElement);
		PRLX.renderer.shadowMap.enabled = true;
		PRLX.setSize();
		//Камера
		PRLX.camera = new THREE.PerspectiveCamera(55,PRLX.size.w/PRLX.size.h, .1, 10000);
		PRLX.camera.position.z = PRLX.cameraOrigin + PRLX.cameraDistance;
		PRLX.camera.lookAt(new THREE.Vector3());
		PRLX.scene.add(PRLX.camera);
		PRLX.renderer.setClearColor(PRLX.background, PRLX.backgroundAlpha);
		//Свет
		PRLX.hemy = new THREE.HemisphereLight( 0xffffff, 0xffffff, PRLX.hemyIntens );
		PRLX.scene.add( PRLX.hemy );
		PRLX.dl = new THREE.DirectionalLight( 0xffffff, PRLX.sunIntens );
		PRLX.dl.castShadow = true;
		PRLX.scene.add( PRLX.dl );
		PRLX.setsunAngle(PRLX.sunAngle);
	
		//Смена размера
		window.addEventListener('resize',PRLX.resize);
		//Процесс загрузки ресурсов
		for(var i = 0; i < data.tImg.length; i++)
		{
			PRLX.textures.push(null);
		}
		for(var i = 0; i < data.dImg.length; i++)
		{
			PRLX.displacement.push(null);
		}
		checkResourceLoaing();
		function checkResourceLoaing ()
		{
			
			for(var i = 0; i < PRLX.textures.length; i++)
			{
				if(PRLX.textures[i] === null)
				{
					PRLX.textures[i] = loadResource(data.tImg[i],PRLX.textures,i);
					return;
				}
			}
			
			for(var i = 0; i < PRLX.displacement.length; i++)
			{
				if(PRLX.displacement[i] === null)
				{
					PRLX.displacement[i] = loadResource(data.dImg[i],PRLX.displacement,i);
					return;
				}
			}
			
			
			PRLX.createScene();
			PRLX.startRender();
			PRLX.loadingComplete();
		}
		
		function loadResource (t,pool,i)
		{
			
			var loader = new THREE.TextureLoader();
			return loader.load(t,function(texture){
				texture.anisotropy = 16;
				texture.needsUpdate = true;
				pool[i] = texture;
				checkResourceLoaing();
				
			});
		}
		
	},
	loadingProcess : function (content)
	loadingComplete : function (){},
	removeScene : function ()
	{
		for(var i = 0; i < PRLX.scene.children.length; i++)
		{
			if(PRLX.scene.children[i].type === "Mesh")
			{
				PRLX.scene.remove(PRLX.scene.children[i]);
			}
		}
	},
	setsunAngle: function(a)
	{
		PRLX.dl.position.x = 150*Math.sin(a-3.14); 
		PRLX.dl.position.z = 150*Math.cos(a-3.14);
		PRLX.sunAngle = a;
	},
	createScene : function ()
	{
		PRLX.depthData = [];
		for(var i = 0; i < PRLX.textures.length; i++)
		{
			PRLX.createPlane(PRLX.textures[i],PRLX.displacement[i],i);
		}
		PRLX.caclOffsetPool();
	},
	wireframe:false,
	createPlane : function(t,d,z)
	{
		var bMap = false;
		
		if(d)
		{
			dMap = d;
		}
		var geometry = new THREE.PlaneGeometry( t.image.width/2, t.image.height/2, PRLX.subdiv,PRLX.subdiv);		

		var material = new THREE.MeshStandardMaterial({
			map:t,
			transparent:true,
			roughness:1,
			metalness:0,
			alphaTest:PRLX.alphaT,
			blendDstAlpha:.5,
			displacementMap: dMap,
			displacementScale: PRLX.depth,
			wireframe: PRLX.wireframe,

			});
		var plane = new THREE.Mesh( geometry, material );
		plane.position.z = z;
		PRLX.scene.add(plane);
		PRLX.planes[z] = plane;
		
		var canvas = document.createElement('canvas');

		canvas.width = dMap.image.width;
		canvas.height = dMap.image.height;
		canvas.getContext('2d').drawImage(dMap.image, 0, 0, dMap.image.width, dMap.image.height);
		PRLX.depthData.push(canvas.getContext('2d'));
	},
	offsetPool :[],
	layersCount:10,
	deformOffset: 69,
	caclOffsetPool : function ()
	{
		
		PRLX.offsetPool = [];
		var layersCount = PRLX.layersCount;
		for(var i = 0; i < PRLX.planes.length; i++)
		{
			PRLX.offsetPool[i] = [];
			for(var j = 0; j < layersCount; j++)
			{
				PRLX.offsetPool[i][j] = [];
			}
			
		}
		
		var layersRangePool = [];
		var oneLayerRange = parseInt((255 - PRLX.deformOffset)/layersCount);
		for(var i = 1; i <= layersCount; i++)
		{
			var layerValue = PRLX.deformOffset + oneLayerRange*(i);
			if(layerValue <= 255)layersRangePool.push(layerValue);
		}
		
		for(var i = 0; i < PRLX.planes.length; i++)
		{
			var vVSp = parseInt(PRLX.depthData[i].canvas.width/PRLX.subdiv);
			var length = PRLX.planes[i].geometry.vertices.length;
			var vertexArr = PRLX.planes[i].geometry.vertices;
			var red = PRLX.depthData[i].createImageData(5,5);
			for(var c = 0; c < red.data.length; c+=4)
			{
				red.data[c+3] = 255;
			}

			var data = PRLX.depthData[i].data;
			var vertexCount = 0;
			for(var y = 0; y < PRLX.subdiv; y++)
			{
				for(var x = 0; x < PRLX.subdiv; x++)
				{
					
					var data = PRLX.depthData[i].getImageData(x*vVSp, y*vVSp,1,1).data;
					var plusC = 0; 
					
					for(var c = 0; c < red.data.length; c+=4)
					{
						
						red.data[c] = data[0];

						
					}
					
					
					if(data[0] > PRLX.deformOffset)
					{
						
						var layerNum = 1;
						for(var j = 0; j < layersCount; j++)
						{
							if(data[0] < layersRangePool[j])
							{
								PRLX.depthData[i].putImageData(red, x*vVSp-1, y*vVSp-1);
								layerNum = j;
								break;
							}							
						}
						var vector6 = vertexArr[vertexCount];
						if(!vector6.oldX)vector6.oldX = vector6.x;
						if(!vector6.oldY)vector6.oldY = vector6.y;
						PRLX.offsetPool[i][layerNum].push(vertexArr[vertexCount]);
						
					}
					vertexCount++;
				}
				vertexCount++;
			}
			
		}

	},
	stopRender : function ()
	{
		PRLX.canRender = false;
	},
	startRender : function()
	{
		PRLX.canRender = true;
		PRLX.calcMSPF();
		PRLX.Render();
		window.addEventListener("mousemove",PRLX.mouseMove);
	},
	size : {w:0,h:0},
	setSize : function(W,H)
	{
		if(!W)W = PRLX.host.offsetWidth;
		if(!H)H = PRLX.host.offsetHeight;
		PRLX.size.w = W;
		PRLX.size.h = H;
		PRLX.renderer.setSize(W, H);
		
	},
	resize : function ()
	{
		var prp = PRLX.host.offsetWidth/PRLX.host.offsetHeight;
		PRLX.setSize();
		PRLX.camera.aspect = prp;
		PRLX.camera.updateProjectionMatrix();

	},
	calcMSPF : function ()
	{
		PRLX.mspf = (1000/PRLX.fps)/1000;
		PRLX.fpsIncur = (PRLX.mspf/100)*10;
	},
	canRender : true,
	deltaTime : 0,
	lastTime : 0,
	currentTime : 0,
	fps: 45,
	fpsIncur: 0,
	mspf : 0,

	Render : function ()
	{	
		

		if(!PRLX.canRender) return;
		PRLX.currentTime = Date.now();
		PRLX.deltaTime = (PRLX.currentTime - PRLX.lastTime)/1000;
		
		if(PRLX.deltaTime >= PRLX.mspf-PRLX.fpsIncur)
		{
			
			PRLX.deltaTime = PRLX.mspf;
			PRLX.lastTime = PRLX.currentTime;
			PRLX.planesOffset(PRLX.mouse);
			PRLX.renderer.render( PRLX.scene, PRLX.camera );
		}
		requestAnimationFrame(PRLX.Render);
	},
	mirror:1,
	angle : 0,
	offset : 0,
	deform : .3,
	planesOffset : function (event)
	{
		
		for(var i = 0; i < PRLX.planes.length; i++)
		{
			var coof = (i+1);
			var offX = ((PRLX.size.w/2 - event.clientX)/100)*PRLX.offset*coof;
			var offY = ((PRLX.size.h/2 - event.clientY)/100)*PRLX.offset*coof;
			var defX = ((PRLX.size.w/2 - event.clientX)/100)*PRLX.deform;
			var defY = ((PRLX.size.h/2 - event.clientY)/100)*PRLX.deform;
			PRLX.planes[i].rotation.y = -((PRLX.size.w/2 - event.clientX)/10000)*PRLX.angle*PRLX.mirror;
			PRLX.planes[i].rotation.x = -((PRLX.size.h/2 - event.clientY)/10000)*PRLX.angle*PRLX.mirror;
			PRLX.planes[i].position.x = offX*PRLX.mirror;
			PRLX.planes[i].position.y = offY*PRLX.mirror;
			var length = PRLX.offsetPool[i].length;
			
			for(var j = 0; j < length; j++)
			{
				var vLength = PRLX.offsetPool[i][j].length;
				for(var k = 0; k < vLength; k++)
				{
					PRLX.offsetPool[i][j][k].x = PRLX.offsetPool[i][j][k].oldX + (defX*PRLX.mirror)*j/10;
					PRLX.offsetPool[i][j][k].y = PRLX.offsetPool[i][j][k].oldY + (defY*PRLX.mirror)*j/10;
					
				}
			}
			PRLX.planes[i].geometry.verticesNeedUpdate = true;
		}
	},
	
	mouse : {clientX:0,clientY:0},
	mouseMove : function(event)
	{
		PRLX.mouse.clientX = event.clientX;
		PRLX.mouse.clientY = event.clientY;
		
	}
	
}
