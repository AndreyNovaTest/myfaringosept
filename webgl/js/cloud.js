var CLOUD = {
	INIT : function (data)
	{
		CLOUD.preloader = data.preloader;
		CLOUD.preloaderIcon = data.preloaderIcon;
		//РЎС†РµРЅР°
		CLOUD.scene = new THREE.Scene();
		//Р РµРЅРґРµСЂ
		CLOUD.renderer = new THREE.WebGLRenderer({antialias:false});
		CLOUD.host = data.host;
		CLOUD.urls = data.urls;
		CLOUD.preloadingEndCb = data.preloadingEndCb;
		CLOUD.host.appendChild(CLOUD.renderer.domElement);
		CLOUD.renderer.shadowMap.enabled = true;
		CLOUD.renderer.setSize( innerWidth, innerHeight );
		//РљР°РјРµСЂР°
		CLOUD.camera = new THREE.PerspectiveCamera(65,innerWidth/innerHeight, .1, 10000);
		CLOUD.scene.add(CLOUD.camera);
		CLOUD.resize();
		CLOUD.renderer.setClearColor(0xF1FBFD);
		CLOUD.renderer.setClearColor(0xCCDDED);
		//РЎРІРµС‚
		CLOUD.hemy = new THREE.HemisphereLight( 0xffffff, 0xaaaaaa, 2.4 );
		CLOUD.scene.add( CLOUD.hemy );
		
	
		//РЎС‚Р°С‚РёСЃС‚РёРєР°
		CLOUD.stats = new Stats();
		//CLOUD.host.appendChild(CLOUD.stats.dom);
		//РЎРјРµРЅР° СЂР°Р·РјРµСЂР°
		window.addEventListener('resize',CLOUD.resize);
		
		//РЎРѕР·РґР°РЅРёРµ canvas
		CLOUD.wSize = {w:1920,h:1080};
		CLOUD.sSize = {w:1024,h:512};
		
		CLOUD.wCanv = document.createElement("canvas");
		CLOUD.wCanv.width = CLOUD.sSize.w;
		CLOUD.wCanv.height = CLOUD.sSize.h;
		CLOUD.wCtx = CLOUD.wCanv.getContext("2d");
		
		CLOUD.sCanv = document.createElement("canvas");
		CLOUD.sCanv.width =  CLOUD.sSize.w;
		CLOUD.sCanv.height = CLOUD.sSize.h;
		CLOUD.sCtx = CLOUD.wCanv.getContext("2d");
		
		CLOUD.mCanv = document.createElement("canvas");
		CLOUD.mCanv.width =  CLOUD.sSize.w;
		CLOUD.mCanv.height = CLOUD.sSize.h;
		CLOUD.mCtx = CLOUD.mCanv.getContext("2d");
		
		CLOUD.dCanv = document.createElement("canvas");
		CLOUD.dCanv.width =  CLOUD.sSize.w;
		CLOUD.dCanv.height = CLOUD.sSize.h;
		CLOUD.dCtx = CLOUD.dCanv.getContext("2d");
		
		//document.body.appendChild(CLOUD.dCanv)
		
		//Р—Р°РіСЂСѓР·РєР° РєРѕРЅС‚РµРЅС‚Р° (content,urls,cbEnd,cbProcess)
		LOADER.INIT(content,CLOUD.urls,CLOUD.startScene,CLOUD.loadingProgress);
	},
	zoomCoof:1,
	resize : function ()
	{
		CLOUD.renderer.setSize(innerWidth, innerHeight);	
		CLOUD.camera.aspect = innerWidth / innerHeight;
		CLOUD.camera.position.z = innerHeight*.74;


		CLOUD.camera.lookAt(new THREE.Vector3());
		CLOUD.camera.updateProjectionMatrix();
	
		CLOUD.resizePlanes();

	},

	preloader:null,
	preloaderIcon:null,
	loadingProgress : function (p)
	{
		CLOUD.preloaderIcon.innerHTML = Math.round(p*100);
	},
	content:null,
	startScene : function (content)
	{
		CLOUD.preloadingEndCb();
		CLOUD.preloader.style.display = "none";
		CLOUD.content = content;
		CLOUD.createBackgroundPlane();
		CLOUD.createCarbordCouds();
		CLOUD.switcwCanvases("wide");
		CLOUD.setTexture(0);
		CLOUD.setDisplacementMap("clouds_0_disp");
		CLOUD.calcOffsetPool();
		window.addEventListener("mousemove",CLOUD.getMousePosition);
		CLOUD.afterShakeAnimation(0);
		CLOUD.render();
		CLOUD.preparePlaneZoom();
	},
	mouse :{clientX:0,clientY:0,},
	getMousePosition : function ()
	{
		
		CLOUD.mouse.clientX = event.clientX;
		CLOUD.mouse.clientY = event.clientY;
		
	},
	planeZoopPool : [],
	preparePlaneZoom : function ()
	{
		/*for(var i = 0; i < 10; i++)
		{
			var canvas = document.createElement("canvas");
			canvas.width =  CLOUD.sSize.w;
			canvas.height = CLOUD.sSize.h;
			var ctx = canvas.getContext("2d");
			ctx.filter = "blur("+i+"px)";
			ctx.drawImage(CLOUD.content.images["plane_1_1"],0,0);
			CLOUD.planeZoopPool.push(canvas);
		}*/
		for(var i = 0; i < 10; i++)
		{
			CLOUD.planeZoopPool.push(CLOUD.content.images["plane_1_1_blur_"+i]);
		}
		
	},
	widthCoof:1,
	heightCoof:1,
	resizePlanes : function ()
	{
		CLOUD.widthCoof = innerWidth/1270;
		CLOUD.heightCoof = innerWidth/720;

		if(CLOUD.plane)
		{
			CLOUD.plane.geometry = CLOUD.createBackGeometry();
			CLOUD.calcOffsetPool();
		}
		if(CLOUD.frontPlane)
		{
			for(var value in CLOUD.frontPlane)
			{
				CLOUD.frontPlane[value].geometry = CLOUD.createFrontGeometry();
			}
			
		}
		if(CLOUD.middlePlane)
		{
			for(var value in CLOUD.middlePlane)
			{
				CLOUD.middlePlane[value].geometry = CLOUD.createMiddleGeometry();
			}
		}
		if(CLOUD.carbordClouds)
		{
			var pPool = CLOUD.getPositionPool();
			for(var i = 0; i < CLOUD.carbordClouds.length; i++)
			{
				CLOUD.carbordClouds[i].geometry = CLOUD.createCarbordGeometry();
				if(CLOUD.activeCarbordPosition)
				{
					CLOUD.carbordClouds[i].position.set(pPool[i][0],pPool[i][1],pPool[i][2]);
				}
				else
				{
					CLOUD.carbordClouds[i].position.set(pPool[i][0],innerHeight,pPool[i][2]);
				}
			
			}
			
		}
	},
	backPlaneCoof : 1,
	createBackGeometry : function ()
	{
		if(innerWidth/innerHeight > 1)
		{
			if(innerWidth/innerHeight > 2) return new THREE.PlaneGeometry( innerWidth*CLOUD.backPlaneCoof, innerWidth/2*CLOUD.backPlaneCoof, CLOUD.subdiv*2,CLOUD.subdiv );
			else return new THREE.PlaneGeometry( innerHeight*2*CLOUD.backPlaneCoof, innerHeight*CLOUD.backPlaneCoof, CLOUD.subdiv*2,CLOUD.subdiv );
			
		}
		else 
		{
			return new THREE.PlaneGeometry( innerHeight*2*CLOUD.backPlaneCoof, innerHeight*CLOUD.backPlaneCoof, CLOUD.subdiv*2,CLOUD.subdiv );
		}
		
	},
	plane : null,
	ratio : 2,
	subdiv: 32,
	dispScale: 1,
	createBackgroundPlane : function (s)
	{
		if(CLOUD.plane)CLOUD.scene.remove(CLOUD.plane);
		var m = new THREE.MeshStandardMaterial({
			map: new THREE.Texture(CLOUD.canv),
			roughness:1,
			
			displacementMap: new THREE.Texture(CLOUD.dCanv),
			displacementScale: CLOUD.dispScale,
			//wireframe:true
			});
		var g = CLOUD.createBackGeometry();
		CLOUD.plane = new THREE.Mesh(g,m);
		CLOUD.plane.position.z = -20;
		CLOUD.plane.rotation.y
		CLOUD.scene.add(CLOUD.plane);
		
	},
	frontPlane : {},
	frontPlanePosition:3,

	createFrontGeometry : function ()
	{
		return new THREE.PlaneGeometry( innerWidth*CLOUD.zoomCoof, innerWidth/2*CLOUD.zoomCoof, 1 );
	},
	createFrontPlane : function (name,texture)
	{
		
		if(CLOUD.frontPlane[name])CLOUD.scene.remove(CLOUD.frontPlane[name]);
		if(!name)return;
		
		var m = new THREE.MeshStandardMaterial({map: new THREE.Texture(CLOUD.content.images[texture]),transparent:true,alphaTest:.1});
		var g = CLOUD.createFrontGeometry();

		 
		CLOUD.frontPlane[name] = new THREE.Mesh(g,m);
		CLOUD.scene.add(CLOUD.frontPlane[name]);
		CLOUD.frontPlane[name].material.map.needsUpdate = true;
	},
	removeFrontPlane : function (name)
	{
		CLOUD.scene.remove(CLOUD.frontPlane[name]);
		delete CLOUD.frontPlane[name];
	},
	middlePlanePosition:2,
	createMiddleGeometry : function ()
	{
		if(innerWidth/innerHeight > 1)
		{
			if(innerWidth/innerHeight > 2) return new THREE.PlaneGeometry( innerWidth, innerWidth/2, 1 );
			else return new THREE.PlaneGeometry( innerHeight*2*CLOUD.zoomCoof, innerHeight*CLOUD.zoomCoof,1 );	
		}
		else 
		{
			if(innerHeight/innerWidth > 2) return new THREE.PlaneGeometry( innerHeight, innerHeight/2, 1 );
			else return new THREE.PlaneGeometry( innerHeight*2*CLOUD.zoomCoof, innerHeight*CLOUD.zoomCoof, 1 );
		}
		
	},
	middlePlane : {},
	
	createMiddlePlane : function (name,texture)
	{
		if(CLOUD.middlePlane[name])CLOUD.scene.remove(CLOUD.middlePlane[name]);
		if(!texture)return;

		var m = new THREE.MeshStandardMaterial({map: new THREE.Texture(CLOUD.content.images[texture]),transparent:true,alphaTest:.2});
		var g = CLOUD.createMiddleGeometry();
		CLOUD.middlePlane[name] = new THREE.Mesh(g,m);
		CLOUD.scene.add(CLOUD.middlePlane[name]);
		CLOUD.middlePlane[name].material.map.needsUpdate = true;
	},
	removeMiddlePlane : function (name)
	{
		CLOUD.scene.remove(CLOUD.middlePlane[name]);
		delete CLOUD.middlePlane[name];
	},

	createCarbordGeometry : function ()
	{
		return new THREE.PlaneGeometry( CLOUD.widthCoof*256, CLOUD.widthCoof*256, 1 );
	},
	activeCarbordPosition : false,
	getPositionPool : function ()
	{
		return [
			[0,innerHeight/2,.005],
			[innerWidth/2.2,innerHeight/5,CLOUD.carbordPosition],
			[-innerWidth/2.3,innerHeight/4,CLOUD.carbordPosition],
			[-innerWidth/5,-innerHeight/2.5,CLOUD.carbordPosition+1],
			[innerWidth/10,-innerHeight/5,CLOUD.carbordPosition],
			[innerWidth/5,-innerHeight/4.5,CLOUD.carbordPosition+2],
			[-innerWidth/2.2,-innerHeight/3.5,CLOUD.carbordPosition+2],
			[innerWidth/4,innerHeight/3.5,CLOUD.carbordPosition],
		];
	},
	carbordClouds : [],
	carbordPosition:2,
	createCarbordCouds : function (texture)
	{
		
		for(var i = 0; i < 8; i++)
		{
			var g = CLOUD.createCarbordGeometry();
			var m = new THREE.MeshStandardMaterial({map: new THREE.Texture(CLOUD.content.images["carbord_cloud_"+i]),transparent:true,alphaTest:.5});
			var cloud = new THREE.Mesh(g,m);
			cloud.visible = false;
			cloud.material.map.needsUpdate = true;
			cloud.position.set(0,0,CLOUD.carbordPosition);
			cloud.add(createLine());
			CLOUD.carbordClouds.push(cloud);
			CLOUD.scene.add(cloud);
		}
		function createLine ()
		{
			var m = new THREE.LineBasicMaterial( { color: 0xffffff, opacity: .3, transparent:true } );
			var g = new THREE.Geometry();
			g.vertices.push(new THREE.Vector3( 0, 0, -1) );
			g.vertices.push(new THREE.Vector3( 0, 10000, -1) );
			g.vertices.push(new THREE.Vector3( 0, 0, -1) );
			var line = new THREE.Line( g, m );
			return line;
		}
	},
	lightnes:null,
	lightnesPosition:4,
	createLigtning: function(type,x,y,s)
	{
	
		var texture = new THREE.Texture(CLOUD.content.images[type])
		var g = new THREE.PlaneGeometry( CLOUD.widthCoof*s, CLOUD.widthCoof*s,1);
		var m = new THREE.MeshStandardMaterial( {
			map: texture,
			transparent:true,
			opacity:0,
			roughness:1,
			emissive:new THREE.Color( 0x000000 ),
			emissiveMap:texture,
			} );
			
		var plane = new THREE.Mesh(g,m);
		plane.position.z = CLOUD.lightnesPosition;
		plane.position.x = x;
		plane.position.y = y;
		texture.needsUpdate = true;
		CLOUD.scene.add( plane );
		
		var pointLight = new THREE.PointLight(0xffffff);
			
		pointLight.intensity = 0;
		pointLight.penumbra = 0;
		pointLight.angle = Math.PI;
		pointLight.decay = 1;
		pointLight.distance = 300;
			
		pointLight.position.set( 0, 0, 100 );
		pointLight.rotation.y = Math.PI*2;
		plane.add(pointLight);

		CLOUD.lightnes = plane;
		CLOUD.spot = pointLight;
		var func = function (count)
		{
			if(count < .5 )
			{
				var c = GUM.deltaCount(0,5,count);
				var c = GUM.lerp(count,0,.5);
				pointLight.intensity = c;
				
				
			}
			plane.material.opacity = count;
			plane.material.emissive.r = count;
			plane.material.emissive.g = count;
			plane.material.emissive.b = count;
			if(count === 1)
			{
				plane.remove( pointLight );
				CLOUD.scene.remove( plane );
			}
		}
		GUM.addAnimation(type+"_lightningAnimation",func,12);
	},
	offsetPool :[],
	layersCount:10,
	deformOffset: 20,
	depthData : {},
	calcOffsetPool : function ()
	{
		var layersCount = CLOUD.layersCount;

		if(CLOUD.offsetPool.length !== 0)
		{
			for(var j = 0; j < CLOUD.offsetPool.length; j++)
			{
				var vLength = CLOUD.offsetPool[j].length;
				for(var k = 0; k < vLength; k++)
				{
					CLOUD.offsetPool[j][k].x = CLOUD.offsetPool[j][k].oldX;
					CLOUD.offsetPool[j][k].y = CLOUD.offsetPool[j][k].oldY;
				}
			}
		}
		/*CLOUD.offsetPool = disp_mask[CLOUD.currentPageNum];
		return;*/
		CLOUD.offsetPool = [];
		for(var j = 0; j < layersCount; j++)
		{
			CLOUD.offsetPool[j] = [];
		}
		var layersRangePool = [];
		var oneLayerRange = parseInt((255 - CLOUD.deformOffset)/layersCount);
		for(var i = 0; i < layersCount; i++)
		{
			var layerValue = CLOUD.deformOffset + oneLayerRange*i;
			if(layerValue <= 255)layersRangePool.push(layerValue);
		}
		
		var vVSp = Math.round(CLOUD.dCanv.width/(CLOUD.subdiv*2));
		var length = CLOUD.plane.geometry.vertices.length;
		var vertexArr = CLOUD.plane.geometry.vertices;
		CLOUD.dCtx.fillStyle  = "red";
		var vertexCount = 0;
		
		for(var y = 1; y < CLOUD.subdiv; y++)
		{
			for(var x = 0; x < CLOUD.subdiv*2; x++)
			{
				
				var data = CLOUD.dCtx.getImageData(x*vVSp, y*vVSp,1,1).data;
				
				
				var plusC = 0; 
				
				if(data[0] > CLOUD.deformOffset)
				{
					var layerNum = layersCount-1;
					for(var j = layersCount-1; j >= 0; j--)	
					{
						if(data[0] > layersRangePool[j])
						{
							layerNum = j;
							
							break;
						}							
					}
					var vector6 = vertexArr[vertexCount];
					if(!vector6.oldX)vector6.oldX = vector6.x;
					else vector6.x = vector6.oldX;
					if(!vector6.oldY)vector6.oldY = vector6.y;
					else vector6.y = vector6.oldY;
					CLOUD.offsetPool[layerNum].push(vertexArr[vertexCount]);
				}
				vertexCount++;
			}
			vertexCount++;
			
		}
	},
	canOffset:true,
	offsetMulti: 50,
	deform:50,
	mirror:1,
	planeOffset : function (event)
	{


		var defX = ((innerWidth/2 - event.clientX)/100)*CLOUD.deform;
		var defY = ((innerHeight/2 - event.clientY)/100)*CLOUD.deform;
		
		var length = -1;
		
		for(var j = 0; j < CLOUD.offsetPool.length; j++)
		{
			var vLength = CLOUD.offsetPool[j].length;
			for(var k = 0; k < vLength; k++)
			{
				CLOUD.offsetPool[j][k].x = CLOUD.offsetPool[j][k].oldX + (defX*CLOUD.mirror)*j/CLOUD.offsetMulti;
				CLOUD.offsetPool[j][k].y = CLOUD.offsetPool[j][k].oldY + (defY*CLOUD.mirror)*j/CLOUD.offsetMulti;
			}
		}
		CLOUD.plane.geometry.verticesNeedUpdate = true;
			
		
	},
	switcwCanvases : function (type)
	{
		if(type === "small")
		{
			CLOUD.plane.material.map.image = CLOUD.sCanv;
		}
		else if(type === "wide")
		{
			CLOUD.plane.material.map.image = CLOUD.wCanv;
		}
		CLOUD.plane.material.map.needsUpdate = true;
	},
	setTexture : function (num)
	{
		
		CLOUD.lastTexture = CLOUD.content.images["clouds_"+num];
		CLOUD.wCtx.drawImage(CLOUD.content.images["clouds_"+num],0,0);
		CLOUD.plane.material.map.image = CLOUD.content.images["w_clouds_"+num];
		CLOUD.plane.material.map.needsUpdate = true;
	},
	setDisplacementMap : function (im)
	{
		CLOUD.dCtx.clearRect(0,0,CLOUD.dCanv.width,CLOUD.dCanv.height)
		CLOUD.dCtx.drawImage(CLOUD.content.images[im],0,0);
		CLOUD.plane.material.displacementMap.needsUpdate = true;
		CLOUD.plane.material.needsUpdate = true;

	},
	prepareTextures : function (img1,img2,dest)
	{
	
		CLOUD.mCtx.clearRect(0,0,CLOUD.content.images["mask_texture"].width,CLOUD.content.images["mask_texture"].height);
		var finalData = CLOUD.mCtx.getImageData(0,0,CLOUD.content.images["mask_texture"].width,CLOUD.content.images["mask_texture"].height);
		var fdata = finalData.data;
		CLOUD.mCtx.drawImage(CLOUD.content.images["mask_texture"],0,0);
		var maskData = CLOUD.mCtx.getImageData(0,0,CLOUD.content.images["mask_texture"].width,CLOUD.content.images["mask_texture"].height);
		var mData = maskData.data;
		CLOUD.mCtx.save();
		CLOUD.mCtx.scale(-1,1);
		CLOUD.mCtx.drawImage(img1,-img1.width,0);
		var im1Data = CLOUD.mCtx.getImageData(0,0,img1.width,img1.height).data;
		CLOUD.mCtx.drawImage(img2,-img2.width,0);
		var im2Data = CLOUD.mCtx.getImageData(0,0,img2.width,img2.height).data;
		
		CLOUD.mCtx.restore();
		var x1,x2,x3;
		var length = mData.length;
		for(var x0 = 0; x0 < length; x0 += 4)
		{
			x1 = x0+1;
			x2 = x0+2;
			x3 = x0+3;
			if(dest < 0)
			{
				fdata[x0] = im1Data[x0] + (im2Data[x0] - im1Data[x0]) * mData[x3]/255;
				fdata[x1] = im1Data[x1] + (im2Data[x1] - im1Data[x1]) * mData[x3]/255;
				fdata[x2] = im1Data[x2] + (im2Data[x2] - im1Data[x2]) * mData[x3]/255;
			}
			else
			{
				fdata[x0] = im2Data[x0] + (im1Data[x0] - im2Data[x0]) * mData[x3]/255;
				fdata[x1] = im2Data[x1] + (im1Data[x1] - im2Data[x1]) * mData[x3]/255;
				fdata[x2] = im2Data[x2] + (im1Data[x2] - im2Data[x2]) * mData[x3]/255;
			}
			fdata[x3] = 255;
		}
		
		CLOUD.mCtx.putImageData(finalData,0,0);
		CLOUD.mCtx.putImageData(finalData,0,0);
		
		var canvas = document.createElement("canvas");
		var oneW = CLOUD.content.images["mask_texture"].width;
		canvas.width = CLOUD.content.images["mask_texture"].width*3;
		canvas.height = CLOUD.content.images["mask_texture"].height;
		var ctx = canvas.getContext("2d");
		
		CLOUD.texturePool = [];
		for(var i = 0; i < 5; i++)
		{
			ctx.filter = "blur("+i+"px)";
			if(dest < 0)
			{
				ctx.drawImage(img1,0,0);
				ctx.drawImage(CLOUD.mCanv,oneW,0);
				ctx.drawImage(img2,oneW*2,0);
			}
			else 
			{
				ctx.drawImage(img2,0,0);
				ctx.drawImage(CLOUD.mCanv,oneW,0);
				ctx.drawImage(img1,oneW*2,0);
			}
			CLOUD.texturePool.push(ctx.getImageData(0,0,CLOUD.content.images["mask_texture"].width*3,CLOUD.content.images["mask_texture"].height));
		}
		
		for(var i = 4; i >= 0; i--)
		{

			CLOUD.texturePool.push(CLOUD.texturePool[i]);
		}
		
	},
	
	lastTexture :null,
	texturePool :[],
	nextTexture :null,
	isAnimating :false,
	animationCallBack : function (){},
	pageAnimation : function (num,cb)
	{
		if(CLOUD.isAnimating)return;
		if(num === CLOUD.lastPageNum)return;
		CLOUD.isAnimating = true;
		CLOUD.animationCallBack = cb;
		CLOUD.shakeAnimation(num);
	},
	planeJiggle : function (plane)
	{
		var func = function (count)
		{
			if(count <= .25)
			{
				var c1 = GUM.deltaCount(count,0,.25);
				c1 = GUM.cos(c1,1);
				
				c1 = GUM.lerp(c1,0,innerHeight/100);
				plane.position.y = c1;
				
			}
			else if(count > .25 && count <= .5)
			{
				var c1 = GUM.deltaCount(count,.25,.5);
				c1 = GUM.cos(c1,0);
				c1 = GUM.lerp(c1,innerHeight/100,0);
				plane.position.y = c1;
			}
			else if(count > .5 && count <= .75)
			{
				var c1 = GUM.deltaCount(count,.5,.75);
				c1 = GUM.cos(c1,1);
				c1 = GUM.lerp(c1,0,-innerHeight/100);
				plane.position.y = c1;
			}
			else if(count > .75 && count <= 1)
			{
				var c1 = GUM.deltaCount(count,.75,1);
				c1 = GUM.cos(c1,0);
				c1 = GUM.lerp(c1,-innerHeight/100,0);
				plane.position.y = c1;
			}
			

		}
		GUM.addAnimation("planeJiggleAnimation",func,300);
	},
	beforeShakeAnimation : function (pageNum)
	{
		
		switch(pageNum)//lastPage
		{
			case 0:
			
			GUM.removeLoop("lightning_1");
			GUM.removeAnimation("lightning_0_1_lightningAnimation",true);
			break;
			case 1:
			GUM.removeLoop("lightning_2");
			GUM.removeLoop("lightning_3");
			GUM.removeAnimation("lightning_1_1_lightningAnimation",true);
			GUM.removeAnimation("lightning_1_2_lightningAnimation",true);
			GUM.removeLoop("planeJiggle");
			if(CLOUD.currentPageNum !== 2)
			{
				CLOUD.frontPlane["plane1"].position.set(0,0,3);
				CLOUD.frontPlane["plane1"].rotation.z = .2;
				var func = function (count)
				{
					var c1 = GUM.cos(count);
					var c2 = GUM.cos(count);
					c1 = GUM.lerp(c1,0,innerWidth*2);
					c2 = GUM.lerp(c2,0,innerHeight);
					CLOUD.frontPlane["plane1"].position.set(c1,c2,3);
					if(count === 1)CLOUD.removeFrontPlane("plane1");
				}
				GUM.addAnimation("planeRightAnimation",func,30);
			}
			else 
			{
				CLOUD.removeFrontPlane("plane1");
			}
			break;
			case 2:
			GUM.removeLoop("planeJiggle");
			if(CLOUD.currentPageNum !== 3)
			{
				
				CLOUD.frontPlane["plane2"].position.set(0,0,CLOUD.frontPlanePosition);
				CLOUD.frontPlane["plane2"].scale.set(.8,.8,1);
				var func = function (count)
				{
					var c1 = GUM.cos(count);
					var c2 = GUM.cos(count);
					c1 = GUM.lerp(c1,0,innerWidth);
					CLOUD.frontPlane["plane2"].position.set(c1,0,CLOUD.frontPlanePosition);
					if(count === 1)CLOUD.removeFrontPlane("plane2");
	
				}
				GUM.addAnimation("planeRightAnimation",func,30);
			}
			else 
			{
				CLOUD.removeFrontPlane("plane2");
			}
			break;
			case 3:
				if(CLOUD.currentPageNum !== 3)
				{
					var func = function (count)
					{
						
						if(count > .4 && count <= .7)
						{
							var c0 = GUM.deltaCount(count,.4,.7);
							c0 = GUM.lerp(c0,1,0)
							CLOUD.middlePlane["woman"].material.opacity = c0;
						}
						if(count === 1)CLOUD.removeMiddlePlane("woman");
					}
					GUM.addAnimation("hideWoman",func,30);
				}
				
			break;
			case 4:
				GUM.removeLoop("planeJiggle");
				var func = function (count)
				{

					CLOUD.middlePlane["plane4"].material.opacity = 1-count;
					if(count === 1)
					{
						CLOUD.removeMiddlePlane("plane4");
					}
				}
				GUM.addAnimation("wingAnimation",func,30);
			break;
			case 5:
			
			break;
			case 6:
			
			break;
			case 7:
				CLOUD.activeCarbordPosition = false;
				var pPool = CLOUD.getPositionPool();
				
				
				var func = function (count)
				{
					
					var c1 = GUM.cos(count);
					c1 = GUM.lerp(c1,0,.02*CLOUD.widthCoof);
					var c2 = GUM.cos(count,0);
					for(var i = 0; i < CLOUD.carbordClouds.length; i++)
					{
						
						cDelta = GUM.lerp(c2,pPool[i][1],innerHeight);
						CLOUD.carbordClouds[i].position.set(pPool[i][0],cDelta,pPool[i][2]);
					}
					CLOUD.frontPlane["boy"].position.set(c1,0,CLOUD.frontPlanePosition);
					CLOUD.frontPlane["boy"].material.opacity = 1-count;
					if(count === 1)
					{
						CLOUD.removeFrontPlane("boy");
					}
					
					
					
				}
				GUM.addAnimation("boyAnimation",func,30);
			break;
			case 8:
			
			break;
			case 9:
				
				
				
				var func = function (count)
				{
					
					
					CLOUD.middlePlane["peoples"].material.opacity = 1-count;
					if(count === 1)
					{
						CLOUD.plane.remove(CLOUD.middlePlane["peoples"]);
						CLOUD.removeMiddlePlane("peoples");
					}
				}
				GUM.addAnimation("peoplesAnimation",func,10);
			break;
		}
	},
	withShakeAnimation : function (pageNum)
	{
		switch(pageNum)//currentPage
		{
			case 0:
			
			break;
			
			case 1:
			
			
			break;
			case 2:
			
			if(CLOUD.lastPageNum === 1)
			{
				
				CLOUD.createFrontPlane("plane2","plane_1_1");
				CLOUD.frontPlane["plane2"].position.set(0,0,CLOUD.frontPlanePosition);
				CLOUD.frontPlane["plane2"].rotation.z = .2;
				var func = function (count)
				{
					var c1 = GUM.cos(count);
					var c2 = c1;
					c1 = GUM.lerp(c1,.2,0);
					c2 = GUM.lerp(c2,1,.8);
					CLOUD.frontPlane["plane2"].rotation.z = c1;
					CLOUD.frontPlane["plane2"].scale.set(c2,c2,1);
				}
				GUM.addAnimation("planeRotateAnimation",func,60);
			}
			else
			{
				CLOUD.createFrontPlane("plane2","plane_1_1");
			
				CLOUD.frontPlane["plane2"].position.set(-3*innerWidth,0,CLOUD.frontPlanePosition);
				CLOUD.frontPlane["plane2"].scale.set(.8,.8,1);
				var func = function (count)
				{
					var c1 = GUM.cos(count);
					var c2 = c1;
					c1 = GUM.lerp(c1,-3*innerWidth,0);
				
					CLOUD.frontPlane["plane2"].position.x = c1;
					
				}
				GUM.addAnimation("planeMoveFromLeftAnimation",func,60);
			}
			break;			
			case 3:
			
				CLOUD.createFrontPlane("plane3","plane_1_1");
				CLOUD.createMiddlePlane("woman","woman");
				CLOUD.middlePlane["woman"].position.set(0,0,CLOUD.middlePlanePosition);
				CLOUD.middlePlane["woman"].material.opacity = 0;
				CLOUD.frontPlane["plane3"].position.set(0,0,CLOUD.frontPlanePosition);
				CLOUD.frontPlane["plane3"].scale.set(.8,.8,1);

				var func = function (count)
				{
					if(count < .7)
					{
						
					var c0 = GUM.deltaCount(count,0,.7);
					var c1 = GUM.cos(c0);
					var c2 = c1;

					c1 = GUM.lerp(c1,0,11.75*-innerWidth);
					c2 = GUM.lerp(c2,.8,47);
					CLOUD.frontPlane["plane3"].position.x = c1;
					
					CLOUD.frontPlane["plane3"].scale.set(c2,c2,1);
					if(count > .1 && count < .5)
					{
						var c5 = Math.round(GUM.deltaCount(count,.1,.5)*9);
						
						CLOUD.frontPlane["plane3"].material.map.image = CLOUD.planeZoopPool[c5];

						CLOUD.frontPlane["plane3"].material.needsUpdate = true;
						CLOUD.frontPlane["plane3"].material.map.needsUpdate = true;
					}
					}
					if(count > .3)
					{
						var c4 = GUM.deltaCount(count,.3,1);
						CLOUD.frontPlane["plane3"].material.opacity = 1 - c4;
						CLOUD.middlePlane["woman"].material.opacity = c4;
					}
				}
				if(CLOUD.lastPageNum === 2)
				{
					GUM.addAnimation("showWomanAnimation",func,45);
				}
				else
				{
					
					CLOUD.frontPlane["plane3"].position.set(-3*innerWidth,0,CLOUD.frontPlanePosition);
					CLOUD.frontPlane["plane3"].scale.set(.8,.8,1);
					var func1 = function (count)
					{
						var c1 = GUM.cos(count);
						var c2 = c1;
						c1 = GUM.lerp(c1,-3*innerWidth,0);
						CLOUD.frontPlane["plane3"].position.x = c1;
						if(count === 1)
						{
							GUM.addAnimation("showWomanAnimation",func,25);
						}
					}
					GUM.addAnimation("planeMoveFromLeftAnimation",func1,10);
				}
			
			break;
			case 4:
				CLOUD.createMiddlePlane("plane4","plane_4_1");
				CLOUD.middlePlane["plane4"].position.set(0,0,1000-CLOUD.middlePlanePosition);
				var func1 = function (count)
				{
					if(count === 1)
					{
						
						CLOUD.middlePlane["plane4"].position.set(0,0,CLOUD.middlePlanePosition);
						CLOUD.middlePlane["plane4"].material.opacity = 0;
						var func2 = function (count)
						{

							if(count > .5)
							{
								var c4 = GUM.deltaCount(count,.5,1);
								CLOUD.middlePlane["plane4"].material.opacity = c4;
							}
						}
						GUM.addAnimation("wingAnimation",func2,30);
					}
				}
				GUM.addAnimation("offsetAnimation",func1,30);
				
			break;
			case 5:
			
			break;
			case 6:
			
			break;
			case 7:
			
			break;
			case 8:
				
			break;
			case 9:
				
				
			break;
			
		}
	},
	afterShakeAnimation : function (pageNum)
	{
		CLOUD.canOffset = true;
		switch(pageNum)
		{
			case 0:
			
			GUM.addLoop("lightning_1",function (){CLOUD.createLigtning("lightning_0_1",innerWidth/4,0,512)},200);
			CLOUD.isAnimating = false;

			break;
			
			case 1:
			
			GUM.addLoop("lightning_2",function (){CLOUD.createLigtning("lightning_1_1",-innerWidth/3.5,-innerHeight/2,512)},450);
			GUM.addLoop("lightning_3",function (){CLOUD.createLigtning("lightning_1_2",innerWidth/3,0,512)},350);
			CLOUD.createFrontPlane("plane1","plane_1_1");
			
			CLOUD.frontPlane["plane1"].position.set(-innerWidth*3,-innerHeight,CLOUD.frontPlanePosition);
			CLOUD.frontPlane["plane1"].rotation.z = .2;
			var func = function (count)
			{
				var c1 = GUM.cos(count);
				var c2 = GUM.cos(count);
				c1 = GUM.lerp(c1,-innerWidth*3,0);
				c2 = GUM.lerp(c2,-innerHeight,0);
				CLOUD.frontPlane["plane1"].position.set(c1,c2,CLOUD.frontPlanePosition);
				if(count === 1)
				{
					CLOUD.isAnimating = false;

					GUM.addLoop("planeJiggle",function (){CLOUD.planeJiggle(CLOUD.frontPlane["plane1"])},300);
				}
			}
			GUM.addAnimation("planeUpAnimation",func,60);
			break;
			case 2:
			CLOUD.isAnimating = false;
			GUM.addLoop("planeJiggle",function (){CLOUD.planeJiggle(CLOUD.frontPlane["plane2"])},300);
			break;			
			case 3:
			CLOUD.isAnimating = false;
			break;
			case 4:
			CLOUD.isAnimating = false;
			
			GUM.addLoop("planeJiggle",function (){CLOUD.planeJiggle(CLOUD.middlePlane["plane4"])},300);
			break;
			case 5:
			CLOUD.isAnimating = false;
			break;
			case 6:
			CLOUD.isAnimating = false;
			break;
			case 7:
				CLOUD.activeCarbordPosition = true;
				var pPool = CLOUD.getPositionPool();
				for(var i = 0; i < CLOUD.carbordClouds.length; i++)
				{
					CLOUD.carbordClouds[i].visible = true;
					CLOUD.carbordClouds[i].position.set(pPool[i][0],innerHeight,pPool[i][2]);
				}
				CLOUD.createFrontPlane("boy","boy");
				CLOUD.frontPlane["boy"].position.set(-.02*innerWidth,0,CLOUD.frontPlanePosition);
				CLOUD.frontPlane["boy"].material.opacity = 0;
				
				var func = function (count)
				{
					var c1 = GUM.cos(count);
					
					c1 = GUM.lerp(c1,-.02*innerWidth,0);
					
					CLOUD.frontPlane["boy"].position.set(c1,0,CLOUD.frontPlanePosition);
					CLOUD.frontPlane["boy"].material.opacity = count;
					for(var j = 0; j < 3; j++)
					{
						if(count <= .175-(j*.1))
						{
							var c2 = GUM.deltaCount(count,0,.175-(j*.1));
							var cDelta = GUM.cos(c2);
							for(var i = 0+j; i < CLOUD.carbordClouds.length; i+=3)
							{
								if(!CLOUD.carbordClouds[i])continue;
								
								var cD = GUM.lerp(cDelta,innerHeight/2,pPool[i][1]-50);
								CLOUD.carbordClouds[i].position.set(pPool[i][0],cD,pPool[i][2]);
							}
						}
						else if(count > .175-(j*.1) && count <= .375-(j*.1))
						{
							var c2 = GUM.deltaCount(count,.175-(j*.1),.375-(j*.1));
							var cDelta = GUM.cos(c2);
							for(var i = 0+j; i < CLOUD.carbordClouds.length; i+=3)
							{
								if(!CLOUD.carbordClouds[i])continue;
								
								var cD = GUM.lerp(cDelta,pPool[i][1]-50,pPool[i][1]+35);
								CLOUD.carbordClouds[i].position.set(pPool[i][0],cD,pPool[i][2]);
							}
						}
						else if(count > .375-(j*.1) && count <= .575-(j*.1))
						{
							var c2 = GUM.deltaCount(count,.375-(j*.1),.575-(j*.1));
							var cDelta = GUM.cos(c2);
							for(var i = 0+j; i < CLOUD.carbordClouds.length; i+=3)
							{
								if(!CLOUD.carbordClouds[i])continue;
								
								var cD = GUM.lerp(cDelta,pPool[i][1]+35,pPool[i][1]-15);
								CLOUD.carbordClouds[i].position.set(pPool[i][0],cD,pPool[i][2]);
							}
						}
						else if(count > .575-(j*.1) && count <= .775-(j*.1))
						{
							var c2 = GUM.deltaCount(count,.575-(j*.1),.775-(j*.1));
							var cDelta = GUM.cos(c2);
							for(var i = 0+j; i < CLOUD.carbordClouds.length; i+=3)
							{
								if(!CLOUD.carbordClouds[i])continue;
								
								var cD = GUM.lerp(cDelta,pPool[i][1]-15,pPool[i][1]+7);
								CLOUD.carbordClouds[i].position.set(pPool[i][0],cD,pPool[i][2]);
							}
						}
						else if(count > .775-(j*.1) && count <= 1-(j*.1))
						{
							var c2 = GUM.deltaCount(count,.775-(j*.1),1-(j*.1));
							var cDelta = GUM.cos(c2);
							for(var i = 0+j; i < CLOUD.carbordClouds.length; i+=3)
							{
								if(!CLOUD.carbordClouds[i])continue;
								
								var cD = GUM.lerp(cDelta,pPool[i][1]+7,pPool[i][1]);
								CLOUD.carbordClouds[i].position.set(pPool[i][0],cD,pPool[i][2]);
							}
						}
					}
					
					if(count === 1)
					{
						
						CLOUD.isAnimating = false;
					}
				}
				GUM.addAnimation("boyAnimation",func,60);
			
			break;
			case 8:
			CLOUD.isAnimating = false;
			break;
			case 9:
				CLOUD.createMiddlePlane("peoples","peoples");
				CLOUD.plane.add(CLOUD.middlePlane["peoples"]);
				CLOUD.middlePlane["peoples"].position.set(0,0,CLOUD.middlePlanePosition);
				CLOUD.middlePlane["peoples"].material.opacity = 0;
				var func = function (count)
				{
					var c4 = GUM.deltaCount(count,.5,1);
					CLOUD.middlePlane["peoples"].material.opacity = c4;
					if(count === 1)
					{
						CLOUD.isAnimating = false;
					}
				}
				GUM.addAnimation("peoplesAnimation",func,20);
			
			break;
			case 10: 
			CLOUD.isAnimating = false;
			break;
		}
	},
	currentPageNum : 0,
	lastPageNum : 0,
	shakeAnimation : function (num)
	{
		CLOUD.canOffset = false,
		CLOUD.currentPageNum = num;
		CLOUD.switcwCanvases("wide");
		CLOUD.nextTexture = CLOUD.content.images["clouds_"+num];
		CLOUD.prepareTextures(CLOUD.lastTexture,CLOUD.nextTexture,CLOUD.lastPageNum-CLOUD.currentPageNum);
		CLOUD.beforeShakeAnimation(CLOUD.lastPageNum);
		CLOUD.withShakeAnimation(CLOUD.currentPageNum);
		function middleAnimation()
		{
			
			CLOUD.setDisplacementMap("clouds_"+(CLOUD.currentPageNum)+"_disp");
			CLOUD.calcOffsetPool();
		}
		function endAnimation ()
		{
			CLOUD.lastTexture = CLOUD.nextTexture;
			CLOUD.lastPageNum = CLOUD.currentPageNum;
			
			
			CLOUD.afterShakeAnimation(CLOUD.currentPageNum);
			CLOUD.animationCallBack();
			
			CLOUD.setTexture(CLOUD.lastPageNum);
		}
		if(CLOUD.lastPageNum-CLOUD.currentPageNum > 0)
		{
			var pageRightDirect = false;
		}
		else var pageRightDirect = true;
		if(CLOUD.lastPageNum === 3)
		{
			var func = function (count)
			{
				if(count < .5)
				{
					var c1 = GUM.deltaCount(count,0,.5);
					c1 = GUM.lerp(c1,CLOUD.dispScale,0);
					CLOUD.plane.material.displacementScale = c1;
					CLOUD.plane.material.needsUpdate = true;
				}
				else if(count < .7 && count >= .65)
				{
					middleAnimation();
				}
				else if(count > .7)
				{
					var c1 = GUM.deltaCount(count,.7,1);
					c1 = GUM.lerp(c1,0,CLOUD.dispScale);
					CLOUD.plane.material.displacementScale = c1;
					
				}
				if(count > .5 && count <= .7)
				{
					var c4 = GUM.deltaCount(count,.5,.7);
					var num = Math.round(c4*9);
					
					if(pageRightDirect)CLOUD.wCtx.putImageData(CLOUD.texturePool[num],0,0);
					else CLOUD.wCtx.putImageData(CLOUD.texturePool[num],-CLOUD.sSize.w*2,0);
					CLOUD.plane.material.map.needsUpdate = true;
				}
				else if(count > .5 && count < .9)
				{
					var c4 = GUM.deltaCount(count,.5,.9);
					
					var num = 9-Math.round(c4*9);
					if(pageRightDirect)CLOUD.wCtx.putImageData(CLOUD.texturePool[num],-CLOUD.sSize.w*2,0);
					else CLOUD.wCtx.putImageData(CLOUD.texturePool[num],0,0);
					CLOUD.plane.material.map.needsUpdate = true;
				}
				
				if(count === 1)
				{
					
					endAnimation();
				}
			}
			GUM.addAnimation("shake",func,30);
		}
		else if(CLOUD.currentPageNum === 9 && CLOUD.lastPageNum === 10)
		{
			middleAnimation();
			endAnimation();
		}
		else if(CLOUD.currentPageNum === 10 && CLOUD.lastPageNum === 9)
		{
			CLOUD.isAnimating = false;
			CLOUD.lastTexture = CLOUD.nextTexture;
			CLOUD.lastPageNum = CLOUD.currentPageNum;
			middleAnimation();
			CLOUD.animationCallBack();
			
		}
		else if(CLOUD.currentPageNum === 3)
		{
			var func = function (count)
			{
				if(count < .3)
				{
					var c1 = GUM.deltaCount(count,0,.3);
					c1 = GUM.lerp(c1,CLOUD.dispScale,0);
					CLOUD.plane.material.displacementScale = c1;
					CLOUD.plane.material.needsUpdate = true;
				}
				else if(count < .7 && count >= .65)
				{
					middleAnimation();
				}
				else if(count > .7)
				{
					var c1 = GUM.deltaCount(count,.7,1);
					c1 = GUM.lerp(0,CLOUD.dispScale,c1);
					CLOUD.plane.material.displacementScale = c1;
					CLOUD.plane.material.needsUpdate = true;
				}
				if(count > .5 && count <= .7)
				{
					var c4 = GUM.deltaCount(count,.5,.7);
					c4 = GUM.cos(c4);
					var num = Math.round(c4*4);
					
					CLOUD.wCtx.putImageData(CLOUD.texturePool[num], 0,0);
					CLOUD.plane.material.map.needsUpdate = true;
				}
				else if(count > .7 && count < .9)
				{
					var c4 = GUM.deltaCount(count,.7,.9);
					c4 = GUM.cos(c4);
					var num = 5+Math.round(c4*4);
					CLOUD.wCtx.putImageData(CLOUD.texturePool[num], -CLOUD.sSize.w*2,0);
					CLOUD.plane.material.map.needsUpdate = true;
				}
				
				if(count === 1)
				{
					
					endAnimation();
				}
			}
			GUM.addAnimation("shake",func,30);
		}
		else
		{
			
			var func = function (count)
			{
				if(count < .3)
				{
					var c1 = GUM.deltaCount(count,0,.3);
					c1 = GUM.lerp(c1,CLOUD.dispScale,0);
					CLOUD.plane.material.displacementScale = c1;
					CLOUD.plane.material.needsUpdate = true;
				}
				else if(count < .7 && count >= .67)
				{
					middleAnimation();
				}
				else if(count > .7)
				{
					var c1 = GUM.deltaCount(count,.7,1);
					c1 = GUM.lerp(c1,0,CLOUD.dispScale);
					CLOUD.plane.material.displacementScale = c1;
					CLOUD.plane.material.needsUpdate = true;
				}
				if(count <= .05)
				{
					if(pageRightDirect)var c0 = GUM.lerp(count,0,.005);
					else var c0 = GUM.lerp(count,0,-.005);
					CLOUD.plane.position.x = c0;
				}
				else if( count > .05 && count <= .3)
				{
					var c1 = GUM.deltaCount(count,.05,.3);
					c1 = GUM.cos(c1);
					if(pageRightDirect)var c11 = GUM.lerp(c1,0,-.2);
					else var c11 = GUM.lerp(c1,0,.2);
					CLOUD.plane.rotation.z = c11;
					
				}
				else if(count > .3 && count <= .5)
				{
					var c2 = GUM.deltaCount(count,.3,.5);
					c2 = GUM.cos(c2);
					if(pageRightDirect)c2 = GUM.lerp(c2,-.2,.1);
					else c2 = GUM.lerp(c2,.2,-.1);
					
					CLOUD.plane.rotation.z = c2;
				}
				else if(count > .5)
				{
					
					var c3 = GUM.deltaCount(count,.5,1);
					c3 = GUM.cos(c3);
					if(pageRightDirect)c3 = GUM.lerp(c3,.1,0);
					else c3 = GUM.lerp(c3,-.1,0);
					CLOUD.plane.rotation.z = c3;
				}
				else if(count > .9)
				{
					var c4 = GUM.deltaCount(count,.9,1);
					c1 = GUM.cos(c1);
					if(pageRightDirect)var c12 = GUM.lerp(c1,.005,0);
					else var c12 = GUM.lerp(c1,-.005,0);
					CLOUD.plane.position.x = c12;
				}
				if(count > .2 && count <= .9)
				{
					var c4 = GUM.deltaCount(count,.2,.9);
					c4 = GUM.cos(c4);
					if(pageRightDirect)c4 = GUM.lerp(c4,0,1);
					else c4 = GUM.lerp(c4,1,0);
					var num = Math.round(c4*9);
					
					CLOUD.wCtx.putImageData(CLOUD.texturePool[num], -CLOUD.sSize.w*c4*2,0);
					CLOUD.plane.material.map.needsUpdate = true;
				}

				if(count > 0 && count < .6)
				{
					var c5 = GUM.deltaCount(count,0,.6);
					c5 = GUM.cos(c5);
					c5 = GUM.lerp(c5,1,2);
					CLOUD.plane.scale.set(c5,c5,c5);
				}
				else if(count > .6)
				{
					var c5 = GUM.deltaCount(count,.6,1);
					c5 = GUM.cos(c5);
					c5 = GUM.lerp(c5,2,1);
					CLOUD.plane.scale.set(c5,c5,c5);
				}
				
				if(count === 1)
				{
					
					endAnimation();
				}
			}
			GUM.addAnimation("shake",func,45);
		}

		
	},
	
	render : function ()
	{
		CLOUD.stats.end();
		CLOUD.stats.begin();
		requestAnimationFrame(CLOUD.render);
		if(CLOUD.canOffset)CLOUD.planeOffset(CLOUD.mouse);
		CLOUD.renderer.render( CLOUD.scene, CLOUD.camera );
		GUM.loopUpdate();
		GUM.update();
	}
	
}