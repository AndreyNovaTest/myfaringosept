var NAVI = {
	
	INIT : function (data)
	{
		NAVI.canvas = document.createElement("canvas");
		NAVI.ctx = NAVI.canvas.getContext("2d");
		NAVI.host = data.host;
		NAVI.btns = data.btns;
		NAVI.canvas.width = NAVI.host.offsetWidth;
		NAVI.canvas.height = NAVI.host.offsetHeight;
		NAVI.host.insertBefore(NAVI.canvas,NAVI.host.firstChild);
		window.addEventListener('resize',NAVI.resize);
		NAVI.resize();
		NAVI.planeIcon = new Image();
		NAVI.planeIcon.src = data.planeScr;
		NAVI.planeIcon.onload = function ()
		{
			NAVI.animatePlane(0);
			NAVI.locateBtns();
		}
		NAVI.host.addEventListener("mouseover",function(event){NAVI.naviPointOver(event,0)});
		NAVI.host.addEventListener("mouseout",function(event){NAVI.naviPointOver(event,1)});
	},
	lineLength:12,
	lineWidth : 1,
	resize:function ()
	{
		NAVI.canvas.width = NAVI.host.offsetWidth;
		NAVI.canvas.height = NAVI.host.offsetHeight;
		NAVI.animatePlane(NAVI.lastNum-1);
		NAVI.locateBtns();

		NAVI.lineWidth = 1;
	},
	mouseIsOver: false,
	naviPointOver : function (event,dest)
	{
		if(dest === 0)NAVI.mouseIsOver = false;
		if(dest === 1 && NAVI.mouseIsOver)return

		if(typeof event === "number")
		{
			var num = event;
		}
		else 
		{
			var num = null;
			for(var i = 0; i < NAVI.btns.length; i++)
			{
				if(event.target === NAVI.btns[i])
				{
					num = i;
					break;
				}
			}
			if(num === null) return;
			num++;
		}
		
		
		var drawLine = function (p1,p2,type,c1,c2)
		{
			if(!c1 && !c2)
			{
				var c1 = NAVI.host.offsetHeight/2;
				var c2 = NAVI.host.offsetHeight/2;
			}
			if(type === 0)
			{
				NAVI.ctx.strokeStyle = "#698FAE";
				NAVI.ctx.lineWidth = NAVI.lineWidth;
			}
			
			else
			{
				NAVI.ctx.strokeStyle = "#1A4671";
				NAVI.ctx.lineWidth = NAVI.lineWidth*2;
			}
			NAVI.ctx.beginPath();
			NAVI.ctx.moveTo(p1,c1);
			NAVI.ctx.lineTo(p2,c2);
			NAVI.ctx.stroke();
		}
		var func = function (count)
		{
			var c = GUM.cos(count);
			var lines = NAVI.lineLength;
			var step = NAVI.host.offsetWidth/lines;
			var center = NAVI.host.offsetHeight/2;
			var radius = NAVI.lineWidth*2;
			NAVI.ctx.clearRect(0,0,NAVI.host.offsetWidth,NAVI.host.offsetHeight);
				
			NAVI.ctx.fillStyle = "#1A4671";
		

			var planeX = 0;
			var planeY = center;
			for(var i = 1; i <= NAVI.btns.length; i++)
			{
				
				NAVI.ctx.beginPath();
				if(dest === 0)
				{
					if(i < num)
					{
						
						var c1 = GUM.lerp(c,i*step,i*step-step/5);
						var c2 = GUM.lerp(c,(i-1)*step,(i-1)*step-step/5);
						NAVI.ctx.arc(c1,center, radius,0, Math.PI*2);
						NAVI.ctx.fill();
						
						if(i > NAVI.lastNum)drawLine(c2,c1,0);
						else drawLine(c2,c1,1);
						
						if(i === NAVI.lastNum)planeX = c1;
					}
					else if(i > num)
					{
						
						var c1 = GUM.lerp(c,i*step,i*step+step/5);
						var c2 = GUM.lerp(c,(i+1)*step,(i+1)*step+step/5);
						NAVI.ctx.arc(c1,center, radius,0, Math.PI*2);
						NAVI.ctx.fill();
						
						if(i >= NAVI.lastNum)drawLine(c2,c1,0);
						else drawLine(c2,c1,1);
						
						if(i === NAVI.lastNum)planeX = c1;
					}
					else
					{
						var c1 = GUM.lerp(c,center,center-step/5);
						var c2 = GUM.lerp(c,(i-1)*step,(i-1)*step-step/5);
						var c3 = GUM.lerp(c,(i+1)*step,(i+1)*step+step/5);
						

						NAVI.ctx.arc(i*step,c1, radius,0, Math.PI*2);
						NAVI.ctx.fill();
						NAVI.ctx.strokeStyle = "#EED13E";
						NAVI.ctx.beginPath();
						NAVI.ctx.arc(i*step,c1, radius*5,0, Math.PI*2);
						NAVI.ctx.stroke();
						
						if(i > NAVI.lastNum)drawLine(c2,i*step,0,center,c1);
						
						else if (i <=  NAVI.lastNum)drawLine(c2,i*step,1,center,c1);
						
						if(i >= NAVI.lastNum)drawLine(c3,i*step,0,center,c1);
						
						else if (i <  NAVI.lastNum)drawLine(c3,i*step,1,center,c1);
						
						
					}
				}
				else
				{
					
					if(i < num)
					{
						var c1 = GUM.lerp(c,i*step-step/5,i*step);
						var c2 = GUM.lerp(c,(i-1)*step-step/5,(i-1)*step);
						
						
						NAVI.ctx.arc(c1,center, radius,0, Math.PI*2);
						NAVI.ctx.fill();
						
						if(i > NAVI.lastNum)drawLine(c2,c1,0);
						else drawLine(c2,c1,1);
						
						if(i === NAVI.lastNum)planeX = c1;
					}
					else if (i > num)
					{
						var c1 = GUM.lerp(c,i*step+step/5,i*step);
						var c2 = GUM.lerp(c,(i+1)*step+step/5,(i+1)*step);
						
						NAVI.ctx.arc(c1,center, radius,0, Math.PI*2);
						NAVI.ctx.fill();
						
						if(i >= NAVI.lastNum)drawLine(c2,c1,0);
						else drawLine(c2,c1,1);
						
						if(i === NAVI.lastNum)planeX = c1;
					}
					else
					{
						var c1 = GUM.lerp(c,center-step/5,center);
						var c2 = GUM.lerp(c,(i-1)*step-step/5,(i-1)*step);
						var c3 = GUM.lerp(c,(i+1)*step+step/5,(i+1)*step);
						
						if(num === NAVI.lastNum)
						{
							planeY = c1;
							planeX = step*i;
						}
						NAVI.ctx.arc(i*step,c1, radius,0, Math.PI*2);
						NAVI.ctx.fill();
						NAVI.ctx.strokeStyle = "#EED13E";
						if(count !== 1 && num !== NAVI.lastNum)
						{
							NAVI.ctx.beginPath();
							NAVI.ctx.arc(i*step,c1, radius*5,0, Math.PI*2);
							NAVI.ctx.stroke();
						}
						
						if(i > NAVI.lastNum)drawLine(c2,i*step,0,center,c1);
						else if (i <=  NAVI.lastNum)drawLine(c2,i*step,1,center,c1);
						
						if(i >= NAVI.lastNum)drawLine(c3,i*step,0,center,c1);
						
						else if (i <  NAVI.lastNum)drawLine(c3,i*step,1,center,c1);
						
						
						
					}
					if(count === 1)
					{
						planeX = NAVI.lastNum*step;
						
					}
				}
			}
			
			if(planeX)NAVI.ctx.drawImage(NAVI.planeIcon,planeX-radius*10/2,planeY-radius*10/2,radius*10,radius*10);
		}
		
		GUM.addAnimation("naviPointUpAnimation",func,10);
		
	},
	btns : [],
	locateBtns : function ()
	{

		var count = NAVI.lineLength;
		var step = NAVI.host.offsetWidth/count;

		var radius = (NAVI.host.offsetWidth/100)*2;
		for(var i = 0; i < NAVI.btns.length; i++)
		{
			NAVI.btns[i].style.left = step*(i+1) - NAVI.btns[i].offsetWidth/2 +'px';
		}
	},
	lastNum : -1,
	drawStaticNaviLine : function (num)
	{
		
		var count = NAVI.lineLength;
		var step = NAVI.host.offsetWidth/count;
		var center = NAVI.host.offsetHeight/2;
		var radius = NAVI.lineWidth*2;
		NAVI.ctx.clearRect(0,0,NAVI.host.offsetWidth,NAVI.host.offsetHeight);
		NAVI.ctx.strokeStyle = "#698FAE";
		NAVI.ctx.lineWidth = NAVI.lineWidth*1;
		
		NAVI.ctx.beginPath();
		NAVI.ctx.moveTo(0,center);
		NAVI.ctx.lineTo(NAVI.host.offsetWidth,center);
		NAVI.ctx.stroke();

		NAVI.ctx.strokeStyle = "#1A4671";
		
		NAVI.ctx.lineWidth = NAVI.lineWidth*2;
		NAVI.ctx.beginPath();
		NAVI.ctx.moveTo(0,center);
		NAVI.ctx.lineTo(step*(num),center);
		NAVI.ctx.stroke();
		
		NAVI.ctx.fillStyle = "#1A4671";
		for(var i = 1; i <= NAVI.btns.length; i++)
		{
			
			NAVI.ctx.arc(i*step,center, radius,0, Math.PI*2);
			NAVI.ctx.fill();
		}
		
	},
	drawPlane : function (coor)
	{
		var center = NAVI.host.offsetHeight/2;
		
	},

	animatePlane : function (num)
	{
		num++;
		var count = NAVI.lineLength;
		var step = NAVI.host.offsetWidth/count;
		var center = NAVI.host.offsetHeight/2;
		var radius = NAVI.lineWidth*2;
		var func = function (count)
		{
			
			NAVI.ctx.clearRect(0,0,NAVI.host.offsetWidth,NAVI.host.offsetHeight);
			NAVI.ctx.strokeStyle = "#698FAE";
			NAVI.ctx.lineWidth = NAVI.lineWidth*1;
			
			NAVI.ctx.beginPath();
			NAVI.ctx.moveTo(0,center);
			NAVI.ctx.lineTo(NAVI.host.offsetWidth,center);
			NAVI.ctx.stroke();
			
			
			
			NAVI.ctx.strokeStyle = "#1A4671";
			NAVI.ctx.lineWidth = NAVI.lineWidth*2;
			NAVI.ctx.beginPath();
			if(num > NAVI.lastNum)
			{
				
				var c = GUM.cos(count);
				
				c = GUM.lerp(c,step*(NAVI.lastNum),step*(num));
				NAVI.ctx.moveTo(0,center);
				NAVI.ctx.lineTo(NAVI.lastNum,center);
				NAVI.ctx.stroke();
				
				NAVI.ctx.moveTo(NAVI.lastNum,center);
				NAVI.ctx.lineTo(c,center);
				NAVI.ctx.stroke();
				
				NAVI.ctx.drawImage(NAVI.planeIcon,c-radius*10/2,center-radius*10/2,radius*10,radius*10);
			}
			else
			{
				var c = GUM.cos(count); 
				c = GUM.lerp(c,step*(NAVI.lastNum),step*(num));
				
				NAVI.ctx.moveTo(0,center);
				NAVI.ctx.lineTo(step*(num),center);
				NAVI.ctx.stroke();
				
				NAVI.ctx.moveTo(NAVI.lastNum,center);
				NAVI.ctx.lineTo(c,center);
				NAVI.ctx.stroke();
				
				NAVI.ctx.drawImage(NAVI.planeIcon,c-radius*10/2,center-radius*10/2,radius*10,radius*10);
			}
			
			
			
			NAVI.ctx.fillStyle = "#1A4671";
			for(var i = 1; i <= NAVI.btns.length; i++)
			{
				
				NAVI.ctx.arc(i*step,center, radius,0, Math.PI*2);
				NAVI.ctx.fill();
			}
			if(count === 1)
			{
				NAVI.lastNum = num;
				NAVI.mouseIsOver = true;
				
			}
		}
		GUM.addAnimation("naviAnimation",func,20);
	}
	
}